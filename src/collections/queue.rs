use super::CAPACITY;
use core::mem::ManuallyDrop;
use core::{fmt, ptr};

pub struct Queue<T> {
    buffer: ManuallyDrop<[T; CAPACITY]>,
    len: usize,
    start: usize,
    end: usize,
}

impl<T> Queue<T> {
    pub const fn new() -> Self {
        Queue {
            buffer: unsafe { super::uninitialized() },
            len: 0,
            start: 0,
            end: 0,
        }
    }

    pub fn push_front(&mut self, item: T) {
        assert!(self.len < CAPACITY, "pushing to full queue");
        self.start = (self.start + CAPACITY - 1) % CAPACITY;
        let place = &mut self.buffer[self.start];
        unsafe {
            ptr::write(place, item);
        }
        self.len += 1;
    }

    pub fn push(&mut self, item: T) {
        assert!(self.len < CAPACITY, "pushing to full queue");
        let place = &mut self.buffer[self.end];
        unsafe {
            ptr::write(place, item);
        }
        self.end = (self.end + 1) % CAPACITY;
        self.len += 1;
    }

    pub fn pop(&mut self) -> Option<T> {
        if self.len > 0 {
            let result = Some(unsafe { ptr::read(&self.buffer[self.start]) });
            self.start = (self.start + 1) % CAPACITY;
            self.len -= 1;
            result
        } else {
            None
        }
    }

    pub fn filter<F: FnMut(&T) -> bool>(&mut self, mut f: F) {
        for _ in 0..(self.len) {
            let item = self.pop().expect("should have got an item");
            if f(&item) {
                self.push(item);
            }
        }
    }

    pub fn len(&self) -> usize {
        self.len
    }
}

impl<T: fmt::Debug> fmt::Debug for Queue<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut list = f.debug_list();
        for i in 0..(self.len) {
            list.entry(&self.buffer[(self.start + i) % CAPACITY]);
        }
        list.finish()
    }
}
