mod fd_map;
mod queue;

pub use self::fd_map::{Fd, FdMap};
pub use self::queue::Queue;

pub const CAPACITY: usize = 100;

const unsafe fn uninitialized<T>() -> T {
    #[allow(unions_with_drop_fields)]
    union Helper<T> {
        t: T,
        u: (),
    }
    Helper { u: () }.t
}
