use core::fmt;
use spark_fs;

const FD_MAP_CAPACITY: usize = 4;

#[derive(Debug, Copy, Clone)]
pub enum Fd {
    Fs(spark_fs::Fd),
    Terminal(usize),
    Null,
}

pub struct FdMap {
    items: [Option<Fd>; FD_MAP_CAPACITY],
}

impl FdMap {
    pub const fn new() -> Self {
        FdMap {
            items: [None; FD_MAP_CAPACITY],
        }
    }

    pub fn has_slots(&self) -> bool {
        self.items.iter().any(|slot| slot.is_none())
    }

    pub fn insert(&mut self, fd: Fd) -> u64 {
        for (index, slot) in self.items.iter_mut().enumerate() {
            if slot.is_none() {
                *slot = Some(fd);
                return index as u64;
            }
        }
        panic!("cannot insert into full map");
    }

    pub fn remove(&mut self, index: u64) -> Option<Fd> {
        if index >= self.items.len() as u64 {
            None
        } else {
            self.items[index as usize].take()
        }
    }

    pub fn remove_one(&mut self) -> Option<Fd> {
        for slot in &mut self.items {
            if slot.is_some() {
                return slot.take();
            }
        }
        None
    }

    pub fn get(&self, index: u64) -> Option<Fd> {
        self.items.get(index as usize).and_then(|&o| o)
    }
}

impl fmt::Debug for FdMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut map = f.debug_map();
        for (i, slot) in self.items.iter().enumerate() {
            if let &Some(ref fd) = slot {
                map.entry(&i, &fd);
            }
        }
        map.finish()
    }
}
