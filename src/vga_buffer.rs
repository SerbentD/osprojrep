//! An abstraction for the VGA buffer.
//!
//! This module implements a terminal-like output which can be easily written to
//! using `print` and `println` macros.

use core::fmt;
use core::ptr::Unique;
use spin::Mutex;

// TODO: Currently there's no way to print colored text from outside of this
// module, so this enum is kept private. Ideally we would want to provide some
// api for colored text and then make `Color` public.
#[allow(dead_code)]
#[repr(u8)]
#[derive(Debug, Clone, Copy)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Debug, Clone, Copy)]
pub struct ColorCode(u8);

impl ColorCode {
    pub const fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct ScreenChar {
    pub ascii_character: u8,
    pub color_code: ColorCode,
}

pub const BUFFER_HEIGHT: usize = 25;
pub const BUFFER_WIDTH: usize = 80;

struct Buffer {
    chars: [[ScreenChar; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

/// Represents a handle for writing to the VGA buffer.
pub struct Writer {
    row_position: usize,
    column_position: usize,
    color_code: ColorCode,
    buffer: Unique<Buffer>,
}

impl Writer {
    /// Write a single char at the current cursor position. Newline (`'\n'`)
    /// will move the cursor to the start of the next line, other characters are
    /// treated as if they were regular printable chars.
    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.column_position >= BUFFER_WIDTH {
                    self.new_line();
                }

                let row = self.row_position;
                let col = self.column_position;

                let color_code = self.color_code;
                self.show_char(
                    row,
                    col,
                    ScreenChar {
                        ascii_character: byte,
                        color_code: color_code,
                    },
                );
                self.column_position += 1;
            }
        }
    }

    fn buffer(&mut self) -> &mut Buffer {
        unsafe { self.buffer.as_mut() }
    }

    fn show_char(&mut self, row: usize, col: usize, ch: ScreenChar) {
        let pos = &mut self.buffer().chars[row][col] as *mut ScreenChar;
        unsafe {
            pos.write_volatile(ch);
        }
    }

    fn new_line(&mut self) {
        if self.row_position == BUFFER_HEIGHT - 1 {
            for row in 1..BUFFER_HEIGHT {
                for col in 0..BUFFER_WIDTH {
                    let character = self.buffer().chars[row][col];
                    self.show_char(row - 1, col, character);
                }
            }
            self.clear_row(BUFFER_HEIGHT - 1);
        } else {
            self.row_position += 1;
        }
        self.column_position = 0;
    }

    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar {
            ascii_character: b' ',
            color_code: self.color_code,
        };
        for col in 0..BUFFER_WIDTH {
            self.show_char(row, col, blank);
        }
    }

    fn clear_screen(&mut self) {
        for row in 0..BUFFER_HEIGHT {
            self.clear_row(row);
        }
        self.row_position = 0;
        self.column_position = 0;
    }

    /// Write a bytestring to the display. Newlines (`'\n'`) will be handled
    /// appropriately, other characters are treated as printable characters.
    pub fn write_bytes(&mut self, bytes: &[u8]) {
        for &byte in bytes {
            self.write_byte(byte);
        }
    }

    pub fn render_raw(
        &mut self,
        terminal: usize,
        buf: &[[ScreenChar; BUFFER_WIDTH]; BUFFER_HEIGHT - 1],
    ) {
        const INDICES: [u8; 12] = *b"123456789ABC";
        for col in 0..BUFFER_WIDTH {
            if col > 6 * 12 {
                self.show_char(
                    0,
                    col,
                    ScreenChar {
                        ascii_character: b' ',
                        color_code: ColorCode::new(Color::LightGray, Color::LightGray),
                    },
                );
                continue;
            }
            let term = col / 6;
            let color = if term == terminal && col % 6 != 0 {
                ColorCode::new(Color::Black, Color::Green)
            } else {
                ColorCode::new(Color::Black, Color::LightGray)
            };
            let ch = if col % 6 == 5 {
                INDICES[term]
            } else {
                b" term"[col % 6]
            };
            self.show_char(
                0,
                col,
                ScreenChar {
                    ascii_character: ch,
                    color_code: color,
                },
            );
        }
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                self.show_char(row, col, buf[row - 1][col]);
            }
        }
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for ch in s.chars() {
            if (ch as u32) < 127 {
                self.write_byte(ch as u8);
            } else {
                self.write_byte(b'?');
            }
        }
        Ok(())
    }
}

/// The handle to write to the VGA buffer.
pub static WRITER: Mutex<Writer> = Mutex::new(Writer {
    row_position: 0,
    column_position: 0,
    color_code: ColorCode::new(Color::White, Color::Black),
    buffer: unsafe { Unique::new_unchecked(0xb8000 as *mut _) },
});

macro_rules! print {
    ($($arg:tt)*) => ({
        $crate::vga_buffer::print(format_args!($($arg)*));
    });
}

macro_rules! println {
    ($fmt:expr) => (print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (print!(concat!($fmt, "\n"), $($arg)*));
}

/// Writes [`core::fmt::Arguments`](core::fmt::Arguments) to the terminal.
///
/// This is a low level function that is used internally in `print` and
/// `println` macros, and is not supposed to be used outside this module.
pub fn print(args: fmt::Arguments) {
    fmt::Write::write_fmt(&mut *WRITER.lock(), args).unwrap();
}

/// Clears the whole VGA buffer and moves the cursor to the top-left corner.
pub fn clear_screen() {
    WRITER.lock().clear_screen();
}
