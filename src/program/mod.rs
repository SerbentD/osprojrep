#![allow(unused)]

use memory::paging::entry::{Entry, EntryFlags};
use memory::paging::{ActivePageTable, InactivePageTable, Mapper, Page, TemporaryPage};
use memory::{Controller, FrameAllocator, PAGE_SIZE};
use syscall::ProcessorState;

pub const CODE_START: usize = 1024 * 1024 * 256; // at 256 MB mark
pub const MAX_CODE_SIZE: usize = 1024 * 1024 * 255; // 255 MB
pub const STACK_START: usize = 1024 * 1024 * 511; // at 511 MB mark
pub const STACK_SIZE: usize = 1024 * 1024; // 1 MB
pub const DATA_START: usize = 1024 * 1024 * 512; // at 512 MB mark
pub const MAX_DATA_SIZE: usize = 1024 * 1024 * 512; // 512 MB, very conservative for now

pub const MAX_OPEN_HANDLES: usize = 8;

pub const MAX_PARAM_LEN: usize = 80;

pub struct ProgramParam {
    buf: [u8; MAX_PARAM_LEN],
    len: usize,
}

impl ProgramParam {
    pub fn as_slice(&self) -> &[u8] {
        &self.buf[..self.len]
    }

    pub fn from_slice(slice: &[u8]) -> Option<Self> {
        if slice.len() <= MAX_PARAM_LEN {
            let mut param = ProgramParam {
                buf: [0; MAX_PARAM_LEN],
                len: slice.len(),
            };
            param.buf[..slice.len()].copy_from_slice(slice);
            Some(param)
        } else {
            None
        }
    }
}

pub struct Program {
    page_table: Option<InactivePageTable>,
    code_pages: usize,
    data_pages: usize,
    processor: ProcessorState,
}

impl Program {
    pub fn allocate<A>(
        code_size: u64,
        data_size: u64,
        controller: &mut Controller<A>,
    ) -> Result<Self, ()>
    where
        A: FrameAllocator,
    {
        let Controller {
            ref mut active_table,
            ref mut allocator,
            ref mut temp_page,
        } = *controller;
        if code_size > MAX_CODE_SIZE as u64 || data_size > MAX_DATA_SIZE as u64 {
            return Err(());
        }
        let code_pages = size_in_pages(code_size as usize);
        let stack_pages = size_in_pages(STACK_SIZE);
        let data_pages = size_in_pages(data_size as usize);
        let frame = allocator.allocate().ok_or(())?;
        let mut program_table = InactivePageTable::new(frame, active_table, temp_page, allocator);
        let mut kernel_map_entries: [Entry; 128] = unsafe { ::core::mem::zeroed() };
        unsafe {
            let map = active_table.get_kernel_entries();
            for i in 0..128 {
                kernel_map_entries[i] = ::core::ptr::read(&map[i] as *const _);
            }
        }
        let allocated: Result<_, ()> = active_table.with(&mut program_table, temp_page, |mapper| {
            unsafe {
                let kernel_map = mapper
                    .p4_mut()
                    .next_table_create(0, allocator)
                    .next_table_create(0, allocator)
                    .kernel_copy_hack();
                for i in 0..128 {
                    kernel_map[i] = ::core::ptr::read(&kernel_map_entries[i] as *const _);
                }
            }
            allocate_page_range(mapper, allocator, CODE_START / PAGE_SIZE, code_pages)?;
            allocate_page_range(mapper, allocator, STACK_START / PAGE_SIZE, stack_pages)?;
            allocate_page_range(mapper, allocator, DATA_START / PAGE_SIZE, data_pages)?;
            Ok(())
        });
        if allocated.is_ok() {
            Ok(Program {
                page_table: Some(program_table),
                code_pages,
                data_pages,
                processor: unsafe { ProcessorState::program_initial() },
            })
        } else {
            // TODO: destroy program_table, as we could have run
            // out of frames after successfully allocating some
            panic!("Program allocation failed, proper cleanup is not yet implemented")
        }
    }

    pub fn set_up_permissions(&mut self, _page_table: ActivePageTable) {
        // TODO: for now all pages have all three RWX permissions set
        // on them, so we can get away with doing nothing here
    }

    pub fn write_param(&mut self, param: &[u8]) {
        let mut param_pos = DATA_START - 0x100;
        unsafe {
            let pos = ::core::slice::from_raw_parts_mut(param_pos as *mut u8, param.len());
            pos.copy_from_slice(param);
        }
        self.processor.rsp -= 0x100;
        self.processor.rax = param_pos as u64;
        self.processor.rbx = param.len() as u64;
    }

    pub fn destroy<A>(self, controller: &mut Controller<A>)
    where
        A: FrameAllocator,
    {
        let table = self.page_table
            .expect("destroyed program does not have a page table");
        table.destroy(controller);
    }

    pub fn is_range_valid(&self, start: usize, len: usize) -> bool {
        let valid_start = STACK_START;
        let valid_end = STACK_START + STACK_SIZE + self.data_pages * PAGE_SIZE;
        start >= valid_start && start + len <= valid_end
    }

    pub fn switch_to_table<A>(&mut self, controller: &mut Controller<A>) -> InactivePageTable {
        controller
            .active_table
            .switch(self.page_table.take().expect("no table to switch to"))
    }

    pub fn restore_table(&mut self, table: InactivePageTable) {
        assert!(
            self.page_table.is_none(),
            "cannot restore, table already exists"
        );
        self.page_table = Some(table);
    }

    pub fn save_state(&mut self, state: ProcessorState) {
        self.processor = state;
    }

    pub fn write_syscall_result(&mut self, result: u64) {
        self.processor.write_syscall_result(result);
    }

    pub fn get_state(&self) -> ProcessorState {
        self.processor
    }
}

fn size_in_pages(in_bytes: usize) -> usize {
    (in_bytes + PAGE_SIZE - 1) / PAGE_SIZE
}

fn allocate_page_range<A>(
    mapper: &mut Mapper,
    allocator: &mut A,
    from: usize,
    amount: usize,
) -> Result<(), ()>
where
    A: FrameAllocator,
{
    if amount == 0 {
        return Ok(());
    }
    let start_page = Page::containing_address(from * PAGE_SIZE);
    let end_page = Page::containing_address((from + amount - 1) * PAGE_SIZE);
    let flags = EntryFlags::PRESENT | EntryFlags::WRITABLE | EntryFlags::USER_ACCESSIBLE;
    for page in Page::range_inclusive(start_page, end_page) {
        let frame = allocator.allocate().ok_or(())?;
        mapper.map_to(page, frame, flags, allocator);
    }
    Ok(())
}

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub struct Pid(pub usize);
