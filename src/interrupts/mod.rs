mod gdt;

use self::gdt::{Descriptor, Gdt};
use keyboard_driver;
use spin::Once;
use syscall::ProcessorState;
use x86_64::structures::gdt::SegmentSelector;
use x86_64::structures::idt::{ExceptionStackFrame, Idt};
use x86_64::structures::tss::TaskStateSegment;

lazy_static! {
    static ref IDT: Idt = {
        let mut idt = Idt::new();
        unsafe {
            idt.double_fault
                .set_handler_fn(double_fault)
                .set_stack_index(0);
            let timer = timer_trampoline as unsafe extern "x86-interrupt" fn(_);
            idt[32]
                .set_handler_fn(::core::mem::transmute(timer))
                .set_stack_index(0);
            let keyboard = keyboard_trampoline as unsafe extern "x86-interrupt" fn(_);
            idt[33]
                .set_handler_fn(::core::mem::transmute(keyboard))
                .set_stack_index(0);
        }

        idt
    };
}

#[allow(improper_ctypes)]
extern "x86-interrupt" {
    fn timer_trampoline(frame: &mut ExceptionStackFrame);
    fn keyboard_trampoline(frame: &mut ExceptionStackFrame);
}

static TSS: Once<TaskStateSegment> = Once::new();
static GDT: Once<Gdt> = Once::new();

pub fn init(stack_top: usize) {
    let tss = TSS.call_once(|| {
        let mut tss = TaskStateSegment::new();
        let stack_top = ::x86_64::VirtualAddress(stack_top);
        tss.interrupt_stack_table[0] = stack_top;
        tss
    });

    let mut code_selector = SegmentSelector(0);
    let mut tss_selector = SegmentSelector(0);
    let gdt = GDT.call_once(|| {
        let mut gdt = Gdt::new();
        code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        tss_selector = gdt.add_entry(Descriptor::tss_segment(&tss));
        gdt
    });

    gdt.load();

    unsafe {
        ::x86_64::instructions::segmentation::set_cs(code_selector);
        ::x86_64::instructions::tables::load_tss(tss_selector);
    }

    IDT.load();
}

#[no_mangle]
pub extern "C" fn rust_keyboard_handler(state: &ProcessorState) -> ! {
    {
        let mut scheduler = ::scheduler::lock();
        scheduler.save_program_state(*state);
        if let Some(pid) = scheduler.active_pid() {
            scheduler.set_program_state(pid, ::scheduler::NewState::Ready);
        }
    }

    unsafe {
        let mut port60 = ::cpuio::Port::<u8>::new(0x60);
        let val = port60.read();

        ::PICS.lock().notify_end_of_interrupt(0x21);

        keyboard_driver::KEYBOARD_DRIVER
            .lock()
            .handle_raw_scancode(val);
    }
}

#[no_mangle]
pub extern "C" fn rust_timer_handler(state: &ProcessorState) -> ! {
    let mut scheduler = ::scheduler::lock();
    scheduler.save_program_state(*state);
    if let Some(pid) = scheduler.active_pid() {
        scheduler.set_program_state(pid, ::scheduler::NewState::Ready);
    }
    unsafe {
        ::PICS.lock().notify_end_of_interrupt(32);
    }
    scheduler.schedule();
}

extern "x86-interrupt" fn double_fault(_frame: &mut ExceptionStackFrame, _code: u64) {
    println!("double fault");
    loop {}
}
