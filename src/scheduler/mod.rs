use collections::{FdMap, Queue};
use core::mem;
use kernel::{HandlerResult, Waker};
use memory::paging::InactivePageTable;
use memory::{Controller, FrameAllocator};
use program::{Pid, Program};
use spin::{Mutex, MutexGuard, Once};
use syscall::ProcessorState;

pub const MAX_PROGRAMS: usize = 16;

pub struct ProgramSlot {
    pub program: Program,
    owning_terminal: Option<usize>,
    state: ProgramState,
    fd_map: FdMap,
    last_error: ::kernel::Error,
}

impl ProgramSlot {
    pub fn fd_map_mut(&mut self) -> &mut FdMap {
        &mut self.fd_map
    }

    pub fn last_error(&self) -> ::kernel::Error {
        self.last_error
    }
}

pub struct Scheduler<A> {
    ready_list: Queue<Pid>,
    programs: [Option<ProgramSlot>; MAX_PROGRAMS],
    active_program: ActiveProgram,
    controller: Controller<A>,
    all_programs_blocked: bool,
    waiting_wakers: Queue<&'static (Waker + Sync)>,
}

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub enum NewState {
    Prioritized,
    Ready,
    Blocked,
}

enum ActiveProgram {
    /// No program is currently active.
    None,
    /// We are using a program's page table, but
    /// that program is not currently running.
    SomeInactive {
        pid: Pid,
        kernel_table: InactivePageTable,
    },
    /// We are using a program's page table and
    /// that program is currently running.
    SomeActive {
        pid: Pid,
        kernel_table: InactivePageTable,
    },
}

#[derive(Debug, Copy, Clone)]
enum ProgramState {
    Running,
    Ready,
    Blocked,
    KillPending(u64),
}

impl<A: FrameAllocator> Scheduler<A> {
    pub fn new(controller: Controller<A>) -> Self {
        Scheduler {
            ready_list: Queue::new(),
            programs: [
                None, None, None, None, None, None, None, None, None, None, None, None, None, None,
                None, None,
            ],
            active_program: ActiveProgram::None,
            controller,
            all_programs_blocked: true,
            waiting_wakers: Queue::new(),
        }
    }

    pub fn create_program(
        &mut self,
        code_size: u64,
        data_size: u64,
        owning_terminal: Option<usize>,
    ) -> Option<Pid> {
        if let Some(index) = self.get_free_slot() {
            let slot = &mut self.programs[index];
            let pid = Pid(index);
            let program = Program::allocate(code_size, data_size, &mut self.controller);
            if let Ok(program) = program {
                let mut fd_map = FdMap::new();
                if let Some(term) = owning_terminal {
                    fd_map.insert(::collections::Fd::Terminal(term));
                } else {
                    fd_map.insert(::collections::Fd::Null);
                }
                *slot = Some(ProgramSlot {
                    program,
                    owning_terminal,
                    state: ProgramState::Blocked,
                    fd_map,
                    last_error: ::kernel::Error::None,
                });
                return Some(pid);
            } else {
                return None;
            }
        }
        None
    }

    fn get_free_slot(&mut self) -> Option<usize> {
        for (index, slot) in self.programs.iter_mut().enumerate() {
            if slot.is_none() {
                return Some(index);
            }
        }
        None
    }

    pub fn kill_program(&mut self, pid: Pid, exit_code: u64) {
        match self.program_mut(pid)
            .expect("program does not exist 1")
            .state
        {
            ProgramState::Running | ProgramState::Ready => {
                self.destroy_program(pid, exit_code);
            }
            ProgramState::Blocked => {
                self.program_mut(pid)
                    .expect("program does not exist 2")
                    .state = ProgramState::KillPending(exit_code);
            }
            ProgramState::KillPending(_) => {}
        }
    }

    pub fn set_program_state(&mut self, pid: Pid, new_state: NewState) {
        match (
            self.program_mut(pid)
                .expect("program does not exist 3")
                .state,
            new_state,
        ) {
            (ProgramState::KillPending(code), _) => {
                assert!(new_state != NewState::Blocked);
                self.destroy_program(pid, code);
            }
            (ProgramState::Blocked, NewState::Ready) | (ProgramState::Running, NewState::Ready) => {
                self.ready_list.filter(|&p| p != pid);
                self.ready_list.push(pid);
                self.program_mut(pid)
                    .expect("program does not exist 4")
                    .state = ProgramState::Ready;
            }
            (ProgramState::Blocked, NewState::Prioritized)
            | (ProgramState::Running, NewState::Prioritized) => {
                self.ready_list.filter(|&p| p != pid);
                self.ready_list.push_front(pid);
                self.program_mut(pid)
                    .expect("program does not exist 5")
                    .state = ProgramState::Ready;
            }
            (ProgramState::Blocked, NewState::Blocked)
            | (ProgramState::Ready, NewState::Ready)
            | (ProgramState::Ready, NewState::Prioritized) => {}
            (ProgramState::Running, NewState::Blocked)
            | (ProgramState::Ready, NewState::Blocked) => {
                self.ready_list.filter(|&p| p != pid);
                self.program_mut(pid)
                    .expect("program does not exist 6")
                    .state = ProgramState::Blocked;
            }
        }
    }

    fn destroy_program(&mut self, pid: Pid, exit_code: u64) {
        if let Some(owner) = self.program_mut(pid)
            .expect("program does not exist 13")
            .owning_terminal
        {
            ::kernel::KernelProcess::send_delayed_no_schedule(
                &::kernel::TERMINALS[owner],
                ::kernel::term::FreeMsg::ProgramExit(exit_code),
            );
            self.schedule_waker(&::kernel::TERMINALS[owner]);
        }
        while let Some(fd) = self.program_mut(pid)
            .expect("program does not exist 7")
            .fd_map_mut()
            .remove_one()
        {
            match fd {
                ::collections::Fd::Fs(fd) => {
                    ::kernel::KernelProcess::send_delayed_no_schedule(
                        &::kernel::FILE_SYSTEM,
                        ::kernel::fs::FreeMsg::CloseFd(fd),
                    );
                    self.schedule_waker(&::kernel::FILE_SYSTEM);
                }
                ::collections::Fd::Terminal(_) | ::collections::Fd::Null => {}
            }
        }
        self.secretly_destroy_program(pid);
    }

    pub fn secretly_destroy_program(&mut self, pid: Pid) {
        self.switch_from_program(pid);
        self.ready_list.filter(|&p| p != pid);
        self.programs[pid.0]
            .take()
            .unwrap()
            .program
            .destroy(&mut self.controller);
    }

    pub fn switch_to_program(&mut self, pid: Pid, make_active: bool) {
        let kernel_table = match mem::replace(&mut self.active_program, ActiveProgram::None) {
            ActiveProgram::None => {
                let program = self.programs[pid.0].as_mut().unwrap();
                let kernel_table = program.program.switch_to_table(&mut self.controller);
                kernel_table
            }
            ActiveProgram::SomeInactive {
                pid: active_pid,
                kernel_table,
            }
            | ActiveProgram::SomeActive {
                pid: active_pid,
                kernel_table,
            } => {
                if active_pid != pid {
                    let old_table = {
                        let program = self.programs[pid.0].as_mut().unwrap();
                        program.program.switch_to_table(&mut self.controller)
                    };
                    self.program_mut(active_pid)
                        .expect("program does not exist 8")
                        .program
                        .restore_table(old_table);
                }
                kernel_table
            }
        };
        self.active_program = if make_active {
            ActiveProgram::SomeActive { pid, kernel_table }
        } else {
            ActiveProgram::SomeInactive { pid, kernel_table }
        };
    }

    pub fn switch_from_program(&mut self, pid: Pid) {
        match mem::replace(&mut self.active_program, ActiveProgram::None) {
            ActiveProgram::None => {}
            ActiveProgram::SomeActive {
                pid: active_pid,
                kernel_table,
            }
            | ActiveProgram::SomeInactive {
                pid: active_pid,
                kernel_table,
            } => {
                if active_pid == pid {
                    let program_table = self.controller.active_table.switch(kernel_table);
                    self.program_mut(pid)
                        .expect("program does not exist 9")
                        .program
                        .restore_table(program_table);
                    self.active_program = ActiveProgram::None;
                } else {
                    self.active_program = ActiveProgram::SomeInactive {
                        pid: active_pid,
                        kernel_table,
                    };
                }
            }
        }
    }

    pub fn program_mut(&mut self, pid: Pid) -> Option<&mut ProgramSlot> {
        self.programs[pid.0].as_mut()
    }

    pub fn schedule(mut self: MutexGuard<Self>) -> ! {
        if let Some(waker) = self.waiting_wakers.pop() {
            if waker.can_receive() {
                mem::drop(self);
                waker.wake();
            }
        }
        if let Some(pid) = self.ready_list.pop() {
            self.all_programs_blocked = false;
            self.switch_to_program(pid, true);
            let state = {
                let program = self.program_mut(pid).expect("program does not exist 10");
                program.state = ProgramState::Running;
                program.program.get_state()
            };
            mem::drop(self);
            unsafe {
                state.jump();
            }
        } else {
            self.all_programs_blocked = true;
            // unlock mutex, and handle next incoming interrupt
            mem::drop(self);
            unsafe {
                ::x86_64::instructions::interrupts::enable();
            }
            loop {}
        }
    }

    pub fn save_program_state(&mut self, state: ProcessorState) {
        if let Some(pid) = self.active_pid() {
            self.program_mut(pid)
                .expect("program does not exist 11")
                .program
                .save_state(state);
        }
    }

    pub fn active_pid(&self) -> Option<Pid> {
        if self.all_programs_blocked {
            return None;
        }
        match self.active_program {
            ActiveProgram::None | ActiveProgram::SomeInactive { .. } => {
                panic!("cannot get active pid when there is no active program");
            }
            ActiveProgram::SomeActive { pid, .. } => Some(pid),
        }
    }

    pub fn write_syscall_result(&mut self, pid: Pid, res: HandlerResult) {
        let program = self.program_mut(pid).expect("program does not exist 12");
        program.program.write_syscall_result(res.value());
        program.last_error = res.error();
    }

    pub fn controller_mut(&mut self) -> &mut Controller<A> {
        &mut self.controller
    }

    pub fn schedule_waker(&mut self, waker: &'static (Waker + Sync)) {
        self.waiting_wakers.push(waker);
    }
}

impl Scheduler<::memory::frame_allocators::Alloc> {
    pub fn write_stats(&mut self, fmt: &mut impl ::core::fmt::Write) {
        let mem_stats = self.controller.allocator.memory_stats();
        let _ = writeln!(fmt, "total bytes: {}", mem_stats.total);
        let _ = writeln!(fmt, "used bytes:  {}", mem_stats.used);
    }
}

pub type DefaultFrameAllocator = ::memory::frame_allocators::Alloc;

static SCHEDULER: Once<Mutex<Scheduler<DefaultFrameAllocator>>> = Once::new();

pub fn init(controller: Controller<DefaultFrameAllocator>) {
    SCHEDULER.call_once(|| Mutex::new(Scheduler::new(controller)));
}

pub fn lock() -> MutexGuard<'static, Scheduler<DefaultFrameAllocator>> {
    SCHEDULER
        .try()
        .expect("scheduler was not yet initialized")
        .lock()
}
