#[no_mangle]
pub extern "C" fn rust_syscall_entry_point(params: &SyscallParams) -> ! {
    ::scheduler::lock().save_program_state(params.return_state);
    match params.index {
        0 => {
            super::exit(params.params[0]);
        }

        2 => {
            super::create(params.params[0], params.params[1]);
        }
        3 => {
            super::write(params.params[0], params.params[1], params.params[2]);
        }
        4 => {
            super::open(params.params[0], params.params[1]);
        }
        5 => {
            super::read(params.params[0], params.params[1], params.params[2]);
        }
        6 => {
            super::close(params.params[0]);
        }
        7 => {
            super::last_error();
        }
        other => {
            super::invalid(other);
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct SyscallParams {
    index: u64,
    params: [u64; 4],
    return_state: ProcessorState,
}

/// Represents processor state.
///
/// This struct is given to syscall handler and is the state that user program
/// saw just before executing the system call.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct ProcessorState {
    pub rax: u64,
    pub rbx: u64,
    pub rcx: u64,
    pub rdx: u64,
    pub rsi: u64,
    pub rdi: u64,
    pub rbp: u64,
    pub rsp: u64,
    pub r8: u64,
    pub r9: u64,
    pub r10: u64,
    pub r11: u64,
    pub r12: u64,
    pub r13: u64,
    pub r14: u64,
    pub r15: u64,
    pub rflags: u64,
    pub rip: u64,
}

impl ProcessorState {
    /// State in which user programs begin execution.
    pub unsafe fn program_initial() -> Self {
        use program;
        ProcessorState {
            rax: 0,
            rbx: 0,
            rcx: 0,
            rdx: 0,
            rsi: 0,
            rdi: 0,
            rbp: 0,
            rsp: (program::STACK_START + program::STACK_SIZE) as u64,
            r8: 0,
            r9: 0,
            r10: 0,
            r11: 0,
            r12: 0,
            r13: 0,
            r14: 0,
            r15: 0,
            rflags: 0,
            rip: program::CODE_START as u64,
        }
    }

    /// Jump to this state, but remaing in kernel mode.
    pub unsafe fn kernel_jump(self) -> ! {
        kernel_process_return(&self);
    }

    /// Jump to this state.
    pub unsafe fn jump(self) -> ! {
        syscall_return(&self);
    }

    /// Return from syscall.
    ///
    /// This function is the same as `ProcessorState::jump`, but also sets
    /// register rax (which holds system call's return value) to the given
    /// result.
    pub unsafe fn syscall_return(mut self, result: u64) -> ! {
        self.rax = result;
        self.jump()
    }

    pub fn write_syscall_result(&mut self, result: u64) {
        self.rax = result;
    }
}

extern "C" {
    fn syscall_return(state: &ProcessorState) -> !;
    fn kernel_process_return(state: &ProcessorState) -> !;
}
