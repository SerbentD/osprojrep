pub mod raw;

pub use self::raw::ProcessorState;
use kernel::{Error, HandlerResult};

const SEGFAULT_EXIT_CODE: u64 = 98648238476;

fn exit(code: u64) -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    scheduler.kill_program(active_program, code);
    scheduler.schedule();
}
/*
fn debug_write(byte: u8) -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    scheduler.set_program_state(active_program, ::scheduler::NewState::Blocked);
    ::core::mem::drop(scheduler);
    //::kernel::KernelProcess::lock_and_send(
    //    &::kernel::DUMMY,
    //    active_program,
    //    ::kernel::dummy::Message::Write(byte),
    //    &mut [],
    //);
}
*/
fn create(path_start: u64, path_len: u64) -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    let start = path_start as usize;
    let len = path_len as usize;
    if !scheduler
        .program_mut(active_program)
        .expect("program does not exist 20")
        .program
        .is_range_valid(start, len)
    {
        // program gave invalid memory range, just kill it
        scheduler.kill_program(active_program, SEGFAULT_EXIT_CODE);
        scheduler.schedule();
    }
    let path = ::spark_fs::Path::from_ascii_str(unsafe {
        ::core::slice::from_raw_parts(start as *const _, len)
    });
    scheduler.set_program_state(active_program, ::scheduler::NewState::Blocked);
    match path {
        Some(path) => {
            ::core::mem::drop(scheduler);
            ::kernel::KernelProcess::lock_and_send(
                &::kernel::FILE_SYSTEM,
                active_program,
                ::kernel::fs::ProgMsg::Create(path),
                &mut [],
            );
        }
        None => {
            scheduler
                .write_syscall_result(active_program, HandlerResult::failure(Error::InvalidPath));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule()
        }
    }
}

fn write(fd: u64, buf_start: u64, buf_len: u64) -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    let start = buf_start as usize;
    let len = buf_len as usize;
    if !scheduler
        .program_mut(active_program)
        .expect("program does not exist 21")
        .program
        .is_range_valid(start, len)
    {
        //panic!("bad range: {}, {}", start, len);
        // program gave invalid memory range, just kill it
        scheduler.kill_program(active_program, SEGFAULT_EXIT_CODE);
        scheduler.schedule();
    }
    let data = unsafe { ::core::slice::from_raw_parts_mut(start as *mut _, len) };
    let fd = match scheduler
        .program_mut(active_program)
        .expect("program does not exist 22")
        .fd_map_mut()
        .get(fd)
    {
        Some(fd) => fd,
        None => {
            scheduler
                .write_syscall_result(active_program, HandlerResult::failure(Error::InvalidFd));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule();
        }
    };
    scheduler.set_program_state(active_program, ::scheduler::NewState::Blocked);
    match fd {
        ::collections::Fd::Fs(fd) => {
            ::core::mem::drop(scheduler);
            ::kernel::KernelProcess::lock_and_send(
                &::kernel::FILE_SYSTEM,
                active_program,
                ::kernel::fs::ProgMsg::Write(fd),
                data,
            );
        }
        ::collections::Fd::Terminal(index) => {
            ::core::mem::drop(scheduler);
            ::kernel::KernelProcess::lock_and_send(
                &::kernel::TERMINALS[index],
                active_program,
                ::kernel::term::ProgMsg::Write,
                data,
            );
        }
        ::collections::Fd::Null => {
            scheduler.write_syscall_result(active_program, HandlerResult::success(buf_len));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule();
        }
    }
}

fn open(path_start: u64, path_len: u64) -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    let start = path_start as usize;
    let len = path_len as usize;
    if !scheduler
        .program_mut(active_program)
        .expect("program does not exist 23")
        .program
        .is_range_valid(start, len)
    {
        // program gave invalid memory range, just kill it
        scheduler.kill_program(active_program, SEGFAULT_EXIT_CODE);
        scheduler.schedule();
    }
    let path = ::spark_fs::Path::from_ascii_str(unsafe {
        ::core::slice::from_raw_parts(start as *const _, len)
    });
    scheduler.set_program_state(active_program, ::scheduler::NewState::Blocked);
    match path {
        Some(path) => {
            ::core::mem::drop(scheduler);
            ::kernel::KernelProcess::lock_and_send(
                &::kernel::FILE_SYSTEM,
                active_program,
                ::kernel::fs::ProgMsg::Open(path),
                &mut [],
            );
        }
        None => {
            scheduler
                .write_syscall_result(active_program, HandlerResult::failure(Error::InvalidPath));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule()
        }
    }
}

fn read(fd: u64, buf_start: u64, buf_len: u64) -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    let start = buf_start as usize;
    let len = buf_len as usize;
    if !scheduler
        .program_mut(active_program)
        .expect("program does not exist 24")
        .program
        .is_range_valid(start, len)
    {
        // program gave invalid memory range, just kill it
        scheduler.kill_program(active_program, SEGFAULT_EXIT_CODE);
        scheduler.schedule();
    }
    let data = unsafe { ::core::slice::from_raw_parts_mut(start as *mut _, len) };
    let fd = match scheduler
        .program_mut(active_program)
        .expect("program does not exist 26")
        .fd_map_mut()
        .get(fd)
    {
        Some(fd) => fd,
        None => {
            scheduler
                .write_syscall_result(active_program, HandlerResult::failure(Error::InvalidFd));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule();
        }
    };
    scheduler.set_program_state(active_program, ::scheduler::NewState::Blocked);
    match fd {
        ::collections::Fd::Fs(fd) => {
            ::core::mem::drop(scheduler);
            ::kernel::KernelProcess::lock_and_send(
                &::kernel::FILE_SYSTEM,
                active_program,
                ::kernel::fs::ProgMsg::Read(fd),
                data,
            );
        }
        ::collections::Fd::Terminal(index) => {
            ::core::mem::drop(scheduler);
            ::kernel::KernelProcess::lock_and_send(
                &::kernel::TERMINALS[index],
                active_program,
                ::kernel::term::ProgMsg::Read,
                data,
            );
        }
        ::collections::Fd::Null => {
            scheduler.write_syscall_result(active_program, HandlerResult::success(buf_len));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule();
        }
    }
}

fn close(fd: u64) -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    let fd = scheduler
        .program_mut(active_program)
        .expect("program does not exist 27")
        .fd_map_mut()
        .remove(fd);
    scheduler.set_program_state(active_program, ::scheduler::NewState::Blocked);
    match fd {
        Some(::collections::Fd::Fs(fd)) => {
            ::core::mem::drop(scheduler);
            ::kernel::KernelProcess::lock_and_send(
                &::kernel::FILE_SYSTEM,
                active_program,
                ::kernel::fs::ProgMsg::Close(fd),
                &mut [],
            );
        }
        Some(::collections::Fd::Null) | Some(::collections::Fd::Terminal(_)) => {
            scheduler.write_syscall_result(active_program, HandlerResult::success(0));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule();
        }
        None => {
            scheduler
                .write_syscall_result(active_program, HandlerResult::failure(Error::InvalidFd));
            scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
            scheduler.schedule();
        }
    }
}

fn last_error() -> ! {
    let mut scheduler = ::scheduler::lock();
    let active_program = scheduler
        .active_pid()
        .expect("syscall called with no active program");
    let error = scheduler
        .program_mut(active_program)
        .expect("program does not exist 28")
        .last_error();
    scheduler.write_syscall_result(active_program, HandlerResult::success(error as u64));
    scheduler.set_program_state(active_program, ::scheduler::NewState::Prioritized);
    scheduler.schedule();
}

fn invalid(id: u64) -> ! {
    println!("program executed invalid syscall: {}", id);
    loop {}
}
