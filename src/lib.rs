#![feature(lang_items)]
#![feature(const_fn)]
#![feature(ptr_internals)]
#![feature(pointer_methods)]
#![feature(abi_x86_interrupt)]
#![feature(arbitrary_self_types)]
#![feature(untagged_unions)]
#![no_std]

extern crate multiboot2;
extern crate rlibc;
extern crate spin;
#[macro_use]
extern crate bitflags;
extern crate x86_64;
#[macro_use]
extern crate lazy_static;
extern crate bit_field;
extern crate cpuio;
extern crate pic8259_simple;
extern crate spark_fs;

#[macro_use]
pub mod vga_buffer;
pub mod collections;
pub mod interrupts;
pub mod kernel;
pub mod keyboard_driver;
pub mod memory;
pub mod program;
pub mod scheduler;
pub mod syscall;

// use memory::heap_allocator::LockedBumpAllocator;
use pic8259_simple::ChainedPics;
use spin::Mutex;

static PICS: Mutex<ChainedPics> = Mutex::new(unsafe { ChainedPics::new(0x20, 0x28) });

const PROGRAMS: &[(&[u8], &[u8])] = &[
    (b"args", include_bytes!("../bin/64/programs/args.bin")),
    (b"cat", include_bytes!("../bin/64/programs/cat.bin")),
    (b"div", include_bytes!("../bin/64/programs/div.bin")),
    (
        b"text",
        b"This is a text file.\nDon't try to execute.\n\nLalalala.\n",
    ),
    (b"echo", include_bytes!("../bin/64/programs/echo.bin")),
    (b"sum", include_bytes!("../bin/64/programs/sum.bin")),
];

#[no_mangle]
pub extern "C" fn rust_main(multiboot_info_ptr: usize, stack_top: usize) -> ! {
    enable_nxe_bit();
    enable_write_protect_bit();
    vga_buffer::clear_screen();

    let boot_info = unsafe { multiboot2::load(multiboot_info_ptr) };

    let controller = memory::init(&boot_info);
    interrupts::init(stack_top);

    unsafe {
        PICS.lock().initialize();
    }

    scheduler::init(controller);

    kernel::KernelProcess::send_delayed(&kernel::FILE_SYSTEM, kernel::fs::FreeMsg::Init);

    kernel::term::set_active_terminal(0);

    scheduler::lock().schedule();
}

fn enable_nxe_bit() {
    use x86_64::registers::msr::{rdmsr, wrmsr, IA32_EFER};
    let nxe_bit = 1 << 11;
    unsafe {
        let efer = rdmsr(IA32_EFER);
        wrmsr(IA32_EFER, efer | nxe_bit);
    }
}

fn enable_write_protect_bit() {
    use x86_64::registers::control_regs as cr;

    unsafe {
        cr::cr0_write(cr::cr0() | cr::Cr0::WRITE_PROTECT);
    }
}

#[lang = "eh_personality"]
#[no_mangle]
pub extern "C" fn eh_personality() {}

#[lang = "panic_fmt"]
#[no_mangle]
pub extern "C" fn panic_fmt(
    msg: core::fmt::Arguments,
    file: &'static str,
    line: usize,
    column: usize,
) -> ! {
    println!("Panic in {}:{}:{}", file, line, column);
    println!("  {}", msg);
    loop {}
}
