use spin::{Mutex, MutexGuard};

#[derive(Debug)]
pub struct Event {
    pub alt: bool,
    pub control: bool,
    pub shift: bool,
    pub capslock: bool,
    pub key: Key,
}

pub trait Handler {
    fn event(&'static self, event: Event) -> !;
}

pub struct KeyboardDriver {
    controlleft: bool,
    controlright: bool,
    altleft: bool,
    altright: bool,
    shiftleft: bool,
    shiftright: bool,
    numlock: bool,
    capslock: bool,
    e0: bool,
    current_handler: Option<&'static (Handler + Sync)>,
}

pub static KEYBOARD_DRIVER: Mutex<KeyboardDriver> = Mutex::new(KeyboardDriver {
    altleft: false,
    altright: false,
    controlleft: false,
    controlright: false,
    numlock: false,
    capslock: false,
    shiftleft: false,
    shiftright: false,
    e0: false,
    current_handler: None,
});

impl KeyboardDriver {
    pub fn register_handler(&mut self, handler: &'static (Handler + Sync)) {
        self.current_handler = Some(handler);
    }

    pub fn handle_raw_scancode(mut self: MutexGuard<Self>, code: u8) -> ! {
        let event = self.scancode_to_event(code);
        if let (Some(event), Some(handler)) = (event, self.current_handler) {
            ::core::mem::drop(self);
            handler.event(event);
        } else {
            ::core::mem::drop(self);
            ::scheduler::lock().schedule();
        }
    }

    fn scancode_to_event(&mut self, x: u8) -> Option<Event> {
        use self::Character::*;
        // for keeping value just for now and reseting for future
        let e0 = self.e0;
        self.e0 = false;
        let key = {
            match x {
                0x01 => Key::Escape,
                0x02 => Key::Character(Num1),
                0x03 => Key::Character(Num2),
                0x04 => Key::Character(Num3),
                0x05 => Key::Character(Num4),
                0x06 => Key::Character(Num5),
                0x07 => Key::Character(Num6),
                0x08 => Key::Character(Num7),
                0x09 => Key::Character(Num8),
                0x0A => Key::Character(Num9),
                0x0B => Key::Character(Num0),
                0x0C => Key::Character(Minus),
                0x0D => Key::Character(Equals),
                0x0E => Key::Backspace,
                0x0F => Key::Tab,
                0x10 => Key::Character(Q),
                0x11 => Key::Character(W),
                0x12 => Key::Character(E),
                0x13 => Key::Character(R),
                0x14 => Key::Character(T),
                0x15 => Key::Character(Y),
                0x16 => Key::Character(U),
                0x17 => Key::Character(I),
                0x18 => Key::Character(O),
                0x19 => Key::Character(P),
                0x1A => Key::Character(LeftBracket),
                0x1B => Key::Character(RightBracket),
                0x1C => Key::Enter,
                0x1D => {
                    self.controlleft = true;
                    Key::Control
                }
                0x1E => Key::Character(A),
                0x1F => Key::Character(S),
                0x20 => Key::Character(D),
                0x21 => Key::Character(F),
                0x22 => Key::Character(G),
                0x23 => Key::Character(H),
                0x24 => Key::Character(J),
                0x25 => Key::Character(K),
                0x26 => Key::Character(L),
                0x27 => Key::Character(Semicolon),
                0x28 => Key::Character(Apostrophe),
                0x29 => Key::Character(Grave), //tilde
                0x2A => {
                    self.shiftleft = true;
                    Key::Shift
                }
                0x2B => Key::Character(Backslash),
                0x2C => Key::Character(Z),
                0x2D => Key::Character(X),
                0x2E => Key::Character(C),
                0x2F => Key::Character(V),
                0x30 => Key::Character(B),
                0x31 => Key::Character(N),
                0x32 => Key::Character(M),
                0x33 => Key::Character(Comma),
                0x34 => Key::Character(Period),
                0x35 => Key::Character(Slash),
                0x36 => {
                    self.shiftright = true;
                    Key::Shift
                }
                0x37 => Key::Pad(PadKey::Multiply),
                0x38 => {
                    self.altleft = true;
                    Key::Alt
                }
                0x39 => Key::Character(Space),
                0x3A => {
                    self.capslock = !self.capslock;
                    Key::CapsLock
                }
                0x3B => Key::F(1),
                0x3C => Key::F(2),
                0x3D => Key::F(3),
                0x3E => Key::F(4),
                0x3F => Key::F(5),
                0x40 => Key::F(6),
                0x41 => Key::F(7),
                0x42 => Key::F(8),
                0x43 => Key::F(9),
                0x44 => Key::F(10),
                0x45 => {
                    self.numlock = true;
                    Key::NumLock
                }
                // SCROLL LOCK
                0x47 => Key::Pad(PadKey::Num(7)),
                0x48 if e0 => Key::Arrow(Arrow::Up),
                0x48 => Key::Pad(PadKey::Num(8)),
                0x49 => Key::Pad(PadKey::Num(9)),
                0x4A => Key::Pad(PadKey::Minus),
                0x4B if e0 => Key::Arrow(Arrow::Left),
                0x4B => Key::Pad(PadKey::Num(4)),
                0x4C => Key::Pad(PadKey::Num(5)),
                0x4D if e0 => Key::Arrow(Arrow::Right),
                0x4D => Key::Pad(PadKey::Num(6)),
                0x4E => Key::Pad(PadKey::Plus),
                0x4F => Key::Pad(PadKey::Num(1)),
                0x50 if e0 => Key::Arrow(Arrow::Down),
                0x50 => Key::Pad(PadKey::Num(2)),
                0x51 => Key::Pad(PadKey::Num(3)),
                0x52 => Key::Pad(PadKey::Num(0)),
                0x53 => Key::Pad(PadKey::Decimal),
                // SYS REQ
                0x57 => Key::F(11),
                0x58 => Key::F(12),

                0x9D => {
                    self.controlleft = false;
                    return None;
                }
                0xAA => {
                    self.shiftleft = false;
                    return None;
                }
                0xB6 => {
                    self.shiftright = false;
                    return None;
                }
                0xB8 => {
                    self.altleft = false;
                    return None;
                }
                0xC5 => {
                    self.numlock = false;
                    return None;
                }
                0xE0 => {
                    self.e0 = true;
                    return None;
                }
                _ => return None,
            }
        };
        Some(Event {
            alt: self.altleft | self.altright,
            control: self.controlleft | self.controlright,
            shift: self.shiftleft | self.shiftright,
            key: key,
            capslock: self.capslock,
        })
    }
}

#[derive(PartialEq, Eq, Copy, Clone, Debug)]
pub enum Character {
    Num1,
    Num2,
    Num3,
    Num4,
    Num5,
    Num6,
    Num7,
    Num8,
    Num9,
    Num0,
    Minus,
    Equals,
    Q,
    W,
    E,
    R,
    T,
    Y,
    U,
    I,
    O,
    P,
    LeftBracket,
    RightBracket,
    A,
    S,
    D,
    F,
    G,
    H,
    J,
    K,
    L,
    Semicolon,
    Apostrophe,
    Grave, // tilde
    Backslash,
    Z,
    X,
    C,
    V,
    B,
    N,
    M,
    Comma,
    Period,
    Slash,
    Space,
}

#[derive(PartialEq, Eq, Copy, Clone, Debug)]
pub enum Key {
    Escape,
    Character(Character),
    Backspace,
    Tab,
    Enter,
    Pad(PadKey),
    F(u8),
    Control,
    Shift,
    Alt,
    CapsLock,
    NumLock,
    Arrow(Arrow),
}

#[derive(PartialEq, Eq, Copy, Clone, Debug)]
pub enum Arrow {
    Up,
    Down,
    Left,
    Right,
}

#[derive(PartialEq, Eq, Copy, Clone, Debug)]
pub enum PadKey {
    Num(u8),
    Multiply,
    Minus,
    Plus,
    Decimal,
}

impl Event {
    pub fn to_char(&self) -> Option<u8> {
        if self.control || self.alt {
            return None;
        }
        let ch = match self.key {
            Key::Pad(PadKey::Num(val)) => return Some(b'0' + val),
            Key::Pad(PadKey::Multiply) => return Some(b'*'),
            Key::Pad(PadKey::Minus) => return Some(b'-'),
            Key::Pad(PadKey::Plus) => return Some(b'+'),
            Key::Pad(PadKey::Decimal) => return Some(b'.'),
            Key::Character(ch) => ch,
            _ => return None,
        };
        use self::Character as C;
        let ch = if !self.shift {
            match ch {
                C::Num1 => b'1',
                C::Num2 => b'2',
                C::Num3 => b'3',
                C::Num4 => b'4',
                C::Num5 => b'5',
                C::Num6 => b'6',
                C::Num7 => b'7',
                C::Num8 => b'8',
                C::Num9 => b'9',
                C::Num0 => b'0',
                C::Minus => b'-',
                C::Equals => b'=',
                C::Q => b'q',
                C::W => b'w',
                C::E => b'e',
                C::R => b'r',
                C::T => b't',
                C::Y => b'y',
                C::U => b'u',
                C::I => b'i',
                C::O => b'o',
                C::P => b'p',
                C::LeftBracket => b'[',
                C::RightBracket => b']',
                C::A => b'a',
                C::S => b's',
                C::D => b'd',
                C::F => b'f',
                C::G => b'g',
                C::H => b'h',
                C::J => b'j',
                C::K => b'k',
                C::L => b'l',
                C::Semicolon => b';',
                C::Apostrophe => b'\'',
                C::Grave => b'`',
                C::Backslash => b'\\',
                C::Z => b'z',
                C::X => b'x',
                C::C => b'c',
                C::V => b'v',
                C::B => b'b',
                C::N => b'n',
                C::M => b'm',
                C::Comma => b',',
                C::Period => b'.',
                C::Slash => b'/',
                C::Space => b' ',
            }
        } else {
            match ch {
                C::Num1 => b'!',
                C::Num2 => b'@',
                C::Num3 => b'#',
                C::Num4 => b'$',
                C::Num5 => b'%',
                C::Num6 => b'^',
                C::Num7 => b'&',
                C::Num8 => b'*',
                C::Num9 => b'(',
                C::Num0 => b')',
                C::Minus => b'_',
                C::Equals => b'+',
                C::Q => b'Q',
                C::W => b'W',
                C::E => b'E',
                C::R => b'R',
                C::T => b'T',
                C::Y => b'Y',
                C::U => b'U',
                C::I => b'I',
                C::O => b'O',
                C::P => b'P',
                C::LeftBracket => b'{',
                C::RightBracket => b'}',
                C::A => b'A',
                C::S => b'S',
                C::D => b'D',
                C::F => b'F',
                C::G => b'G',
                C::H => b'H',
                C::J => b'J',
                C::K => b'K',
                C::L => b'L',
                C::Semicolon => b':',
                C::Apostrophe => b'"',
                C::Grave => b'~',
                C::Backslash => b'|',
                C::Z => b'Z',
                C::X => b'X',
                C::C => b'C',
                C::V => b'V',
                C::B => b'B',
                C::N => b'N',
                C::M => b'M',
                C::Comma => b'<',
                C::Period => b'>',
                C::Slash => b'?',
                C::Space => b' ',
            }
        };
        if self.capslock {
            Some(switch_case(ch))
        } else {
            Some(ch)
        }
    }
}

fn switch_case(ch: u8) -> u8 {
    if ch >= b'a' && ch <= b'z' {
        ch - b'a' + b'A'
    } else if ch >= b'A' && ch <= b'Z' {
        ch - b'A' + b'a'
    } else {
        ch
    }
}
