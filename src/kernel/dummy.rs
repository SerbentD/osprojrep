use super::{HandlerResult, KernelProcess, MessageHandler, Suspender};
use program::Pid;
use spin::Mutex;

#[derive(Debug)]
pub enum Message {
    Write(u8),
}

pub struct Dummy;

impl MessageHandler for Dummy {
    type ProgMsg = Message;
    type FreeMsg = !;

    fn handle(
        &mut self,
        _suspender: &mut Suspender<Self>,
        sender: Pid,
        message: Message,
    ) -> HandlerResult {
        println!("from {:#x}, msg: {:?}", sender.0, message);
        HandlerResult::success(0)
    }

    fn handle_free(&mut self, _suspender: &mut Suspender<Self>, _message: !) {}
}

pub const STACK: u64 = 1024 * 1024 * 200;
pub static DUMMY: Mutex<KernelProcess<Dummy>> = Mutex::new(KernelProcess::new(Dummy, STACK));
