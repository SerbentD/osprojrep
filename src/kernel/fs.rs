use super::{Error, HandlerResult, KernelProcess, MessageHandler, Suspender};
use core::cmp;
use program::{Pid, ProgramParam};
use spark_fs::io::{self, Read, Write};
use spark_fs::{self, Fd, Path};
use spin::Mutex;

const STORAGE_SIZE: usize = 16_777_696;

struct MemoryStorage {
    position: usize,
    storage: [u8; STORAGE_SIZE],
    suspender: Option<&'static mut Suspender<FileSystem>>,
}

impl MemoryStorage {
    const fn new() -> Self {
        MemoryStorage {
            position: 0,
            storage: [0; STORAGE_SIZE],
            suspender: None,
        }
    }

    fn suspend(&mut self) {
        self.suspender
            .as_mut()
            .expect("fs has no suspender")
            .suspend();
    }
}

impl io::Read for MemoryStorage {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let remaining_data = STORAGE_SIZE - self.position;
        let to_read = cmp::min(buf.len(), remaining_data);
        {
            let source = &self.storage[self.position..(self.position + to_read)];
            buf.copy_from_slice(source);
        }
        self.position += to_read;
        // self.suspend();
        Ok(to_read)
    }
}

impl io::Write for MemoryStorage {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let remaining_space = STORAGE_SIZE - self.position;
        let to_write = cmp::min(buf.len(), remaining_space);
        {
            let dest = &mut self.storage[self.position..(self.position + to_write)];
            dest.copy_from_slice(buf);
        }
        self.position += to_write;
        // self.suspend();
        Ok(to_write)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl io::Seek for MemoryStorage {
    fn seek(&mut self, from: io::SeekFrom) -> io::Result<u64> {
        let pos = match from {
            io::SeekFrom::Start(pos) => pos as i64,
            io::SeekFrom::End(pos) => STORAGE_SIZE as i64 + pos,
            io::SeekFrom::Current(delta) => self.position as i64 + delta,
        };
        if pos < 0 || pos >= STORAGE_SIZE as i64 {
            panic!("fs seek out of range");
        }
        self.position = pos as usize;
        Ok(self.position as u64)
    }
}

const INNER_BUF_SIZE: usize = 512;

pub struct FileSystem {
    fs: Option<spark_fs::FileSystem<'static, MemoryStorage>>,
    buf: [u8; INNER_BUF_SIZE],
}

static mut STORAGE: MemoryStorage = MemoryStorage::new();

impl FileSystem {
    const unsafe fn new() -> Self {
        FileSystem {
            fs: None,
            buf: [0; INNER_BUF_SIZE],
        }
    }

    fn on_raw<F, T>(&mut self, suspender: &mut Suspender<Self>, f: F) -> T
    where
        F: FnOnce(&mut spark_fs::FileSystem<'static, MemoryStorage>, &mut [u8; 512]) -> T,
    {
        let inner = self.fs.as_mut().expect("fs used before initialization");
        inner.inner_mut().suspender = Some(unsafe { ::core::mem::transmute(suspender) });
        let result = f(inner, &mut self.buf);
        inner.inner_mut().suspender = None;
        result
    }

    fn init(&mut self, suspender: &mut Suspender<Self>) {
        unsafe {
            STORAGE.suspender = Some(::core::mem::transmute(&mut *suspender));
            self.fs = Some(spark_fs::FileSystem::new(&mut STORAGE).expect("failed to init fs"));
            self.fs.as_mut().unwrap().inner_mut().suspender = None;
        }
        self.on_raw::<_, io::Result<()>>(suspender, |fs, _| {
            let size = STORAGE_SIZE as u64;
            spark_fs::format_storage(fs.inner_mut(), size)?;
            for (name, contents) in ::PROGRAMS {
                let fd = fs.create(Path::from_ascii_str(name).expect("bad builtin file name"))?;
                fs.get_writer(&fd)?.write_all(contents)?;
                fs.close(fd)?;
            }
            Ok(())
        }).expect("failed to format file storage");
    }

    fn create(
        &mut self,
        suspender: &mut Suspender<Self>,
        sender: Pid,
        path: Path,
    ) -> HandlerResult {
        if ::scheduler::lock()
            .program_mut(sender)
            .expect("program does not exist 30")
            .fd_map_mut()
            .has_slots()
        {
            let result = self.on_raw(suspender, |fs, _| fs.create(path));
            match result {
                Ok(fd) => {
                    let program_fd = ::scheduler::lock()
                        .program_mut(sender)
                        .expect("program does not exist 31")
                        .fd_map_mut()
                        .insert(::collections::Fd::Fs(fd));
                    HandlerResult::success(program_fd)
                }
                Err(_) => HandlerResult::failure(Error::Other),
            }
        } else {
            HandlerResult::failure(Error::FdLimit)
        }
    }

    fn write(&mut self, suspender: &mut Suspender<Self>, _sender: Pid, fd: Fd) -> HandlerResult {
        let mut written = 0;
        while written < suspender.data().len() {
            let remaining = suspender.data().len() - written;
            let step = ::core::cmp::min(remaining, INNER_BUF_SIZE);
            {
                let source = &suspender.data()[written..(written + step)];
                self.buf[..step].copy_from_slice(source);
            }
            let result = self.on_raw(suspender, |fs, buf| {
                let mut writer = fs.get_writer(&fd)?;
                writer.write_all(&buf[..step])
            });
            match result {
                Ok(()) => written += step,
                Err(_) => {
                    return HandlerResult::failure(Error::Other);
                }
            }
        }
        HandlerResult::success(written as u64)
    }

    fn open(&mut self, suspender: &mut Suspender<Self>, sender: Pid, path: Path) -> HandlerResult {
        if ::scheduler::lock()
            .program_mut(sender)
            .expect("program does not exist 32")
            .fd_map_mut()
            .has_slots()
        {
            let result = self.on_raw(suspender, |fs, _| fs.open_read(path));
            match result {
                Ok(fd) => {
                    let program_fd = ::scheduler::lock()
                        .program_mut(sender)
                        .expect("program does not exist 33")
                        .fd_map_mut()
                        .insert(::collections::Fd::Fs(fd));
                    HandlerResult::success(program_fd)
                }
                Err(_) => HandlerResult::failure(Error::Other),
            }
        } else {
            HandlerResult::failure(Error::FdLimit)
        }
    }

    fn read(&mut self, suspender: &mut Suspender<Self>, _sender: Pid, fd: Fd) -> HandlerResult {
        let mut read = 0;
        while read < suspender.data().len() {
            let remaining = suspender.data().len() - read;
            let step = ::core::cmp::min(remaining, INNER_BUF_SIZE);
            let result = self.on_raw(suspender, |fs, buf| {
                let mut reader = fs.get_reader(&fd)?;
                reader.read(&mut buf[..step])
            });
            match result {
                Ok(0) => {
                    return HandlerResult::success(read as u64);
                }
                Ok(bytes) => {
                    let source = &self.buf[..step];
                    &suspender.data_mut()[read..(read + step)].copy_from_slice(source);
                    read += bytes;
                }
                Err(_) => {
                    return HandlerResult::failure(Error::Other);
                }
            }
        }
        HandlerResult::success(read as u64)
    }

    fn close(&mut self, suspender: &mut Suspender<Self>, fd: Fd) -> HandlerResult {
        self.on_raw(suspender, |fs, _| match fs.close(fd) {
            Ok(()) => HandlerResult::success(0),
            Err(_) => HandlerResult::failure(Error::Other),
        })
    }

    fn run_program(
        &mut self,
        suspender: &mut Suspender<Self>,
        exe: Path,
        arg: ProgramParam,
        terminal: usize,
        detach: bool,
    ) {
        let result = self.on_raw(suspender, |fs, _| {
            let fd = fs.open_read(exe)?;
            let mut header = [0; 24];
            let res = fs.get_reader(&fd)
                .and_then(|mut r| r.read_exact(&mut header));
            if let Err(e) = res {
                fs.close(fd)?;
                return Err(e);
            }
            if &header[0..8] != b"sparkexe" {
                fs.close(fd)?;
                return Err(io::Error::new(io::ErrorKind::Other, "invalid exe"));
            }
            let code = u64_from_bytes(&header[8..16]);
            let data = u64_from_bytes(&header[16..24]);
            let mut scheduler = ::scheduler::lock();
            let owner = if detach { None } else { Some(terminal) };
            match scheduler.create_program(code, data, owner) {
                Some(pid) => Ok((pid, fd, code, data)),
                None => {
                    fs.close(fd)?;
                    Err(io::Error::new(
                        io::ErrorKind::Other,
                        "cannot allocate program",
                    ))
                }
            }
        });
        let result = match result {
            Ok((pid, fd, code, data)) => {
                ::scheduler::lock().switch_to_program(pid, false);
                unsafe {
                    suspender.handle_program(pid);
                }
                ::scheduler::lock()
                    .program_mut(pid)
                    .expect("program does not exist 39")
                    .program
                    .write_param(arg.as_slice());
                self.on_raw(suspender, |fs, buf| {
                    let res = fs.get_reader(&fd)
                        .and_then(|reader| load_program(reader, code, data, buf));
                    match res {
                        Ok(()) => {
                            fs.close(fd)
                                .expect("failed to close descriptor after loading");
                            Ok(pid)
                        }
                        Err(e) => {
                            ::scheduler::lock().secretly_destroy_program(pid);
                            fs.close(fd)?;
                            Err(e)
                        }
                    }
                })
            }
            Err(e) => Err(e),
        };
        ::kernel::KernelProcess::send_delayed(
            &::kernel::TERMINALS[terminal],
            ::kernel::term::FreeMsg::RunResult(result),
        );
    }
}

fn load_program(
    mut reader: impl io::Read,
    code: u64,
    data: u64,
    buf: &mut [u8; 512],
) -> io::Result<()> {
    let code_range = unsafe {
        ::core::slice::from_raw_parts_mut(::program::CODE_START as *mut _, code as usize)
    };
    let amount = read_range(&mut reader, buf, code_range)? as u64;
    if amount < code {
        return Err(io::Error::new(io::ErrorKind::Other, "cannot read program"));
    }
    let data_range = unsafe {
        ::core::slice::from_raw_parts_mut(::program::DATA_START as *mut _, data as usize)
    };
    let amount = read_range(&mut reader, buf, data_range)?;
    let remaining = unsafe {
        ::core::slice::from_raw_parts_mut(
            (::program::DATA_START + amount) as *mut u8,
            data as usize - amount,
        )
    };
    for byte in remaining {
        *byte = 0;
    }
    Ok(())
}

fn read_range(
    mut reader: impl io::Read,
    buf: &mut [u8; 512],
    output: &mut [u8],
) -> io::Result<usize> {
    let mut read = 0;
    while read < output.len() {
        let remaining = output.len() - read;
        let step = ::core::cmp::min(buf.len(), remaining);
        match reader.read(&mut buf[..step])? {
            0 => return Ok(read),
            x => {
                output[read..(read + x)].copy_from_slice(&buf[..x]);
                read += x;
            }
        }
    }
    Ok(read)
}

fn u64_from_bytes(bytes: &[u8]) -> u64 {
    debug_assert_eq!(bytes.len(), 8);
    let mut res = 0;
    for &byte in bytes.iter().rev() {
        res = res * 256 + byte as u64;
    }
    res
}

pub enum ProgMsg {
    Create(Path),
    Write(Fd),
    Open(Path),
    Read(Fd),
    Close(Fd),
}

pub enum FreeMsg {
    Init,
    CloseFd(Fd),
    Run {
        path: Path,
        arg: ProgramParam,
        terminal: usize,
        detach: bool,
    },
}

impl MessageHandler for FileSystem {
    type ProgMsg = ProgMsg;
    type FreeMsg = FreeMsg;

    fn handle(
        &mut self,
        suspender: &mut Suspender<Self>,
        sender: Pid,
        message: ProgMsg,
    ) -> HandlerResult {
        match message {
            ProgMsg::Create(path) => {
                let result = self.create(suspender, sender, path);
                /*println!(
                    "[ {:?} ] create({}) -> {{ val: {}, err: {:?} }}",
                    sender,
                    path,
                    result.value(),
                    result.error(),
                );*/
                result
            }
            ProgMsg::Write(fd) => {
                let result = self.write(suspender, sender, fd);
                /*println!(
                    "[ {:?} ] write({:?}, {:?}) -> {{ val: {}, err: {:?} }}",
                    sender,
                    fd,
                    suspender.data(),
                    result.value(),
                    result.error(),
                );*/
                result
            }
            ProgMsg::Open(path) => {
                let result = self.open(suspender, sender, path);
                /*println!(
                    "[ {:?} ] open({}) -> {{ val: {}, err: {:?} }}",
                    sender,
                    path,
                    result.value(),
                    result.error(),
                );*/
                result
            }
            ProgMsg::Read(fd) => {
                let result = self.read(suspender, sender, fd);
                /*println!(
                    "[ {:?} ] read({:?}, {}) -> {{ val: {}, err: {:?} }}",
                    sender,
                    fd,
                    suspender.data().len(),
                    result.value(),
                    result.error(),
                );*/
                result
            }
            ProgMsg::Close(fd) => {
                let result = self.close(suspender, fd);
                /*println!(
                    "[ {:?} ] close({:?}) -> {{ val: {}, err: {:?} }}",
                    sender,
                    fd,
                    result.value(),
                    result.error(),
                );*/
                result
            }
        }
    }

    fn handle_free(&mut self, suspender: &mut Suspender<Self>, message: FreeMsg) {
        match message {
            FreeMsg::Init => {
                self.init(suspender);
            }
            FreeMsg::CloseFd(fd) => {
                self.close(suspender, fd);
            }
            FreeMsg::Run {
                path,
                arg,
                terminal,
                detach,
            } => {
                self.run_program(suspender, path, arg, terminal, detach);
            }
        }
    }
}

pub const STACK: u64 = 1024 * 1024 * 201;
pub static FILE_SYSTEM: Mutex<KernelProcess<FileSystem>> =
    Mutex::new(KernelProcess::new(unsafe { FileSystem::new() }, STACK));
