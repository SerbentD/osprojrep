use collections::Queue;
use core::cell::UnsafeCell;
use program::Pid;
use spin::{Mutex, MutexGuard};
use syscall::ProcessorState;

struct ProgMsg<T: MessageHandler> {
    payload: T::ProgMsg,
    data: &'static mut [u8],
    sender: Pid,
}

struct FreeMsg<T: MessageHandler> {
    payload: T::FreeMsg,
}

enum Message<T: MessageHandler> {
    Program(ProgMsg<T>),
    Free(FreeMsg<T>),
}

#[repr(u64)]
#[derive(Debug, Copy, Clone)]
pub enum Error {
    None = 0,
    FdLimit = 1,
    InvalidFd = 2,
    InvalidPath = 3,
    Other = 4,
}

#[derive(Debug, Copy, Clone)]
pub struct HandlerResult {
    inner: Result<u64, Error>,
}

impl HandlerResult {
    pub fn success(value: u64) -> Self {
        HandlerResult { inner: Ok(value) }
    }

    pub fn failure(error: Error) -> Self {
        HandlerResult { inner: Err(error) }
    }

    pub fn value(&self) -> u64 {
        self.inner.unwrap_or(::core::u64::MAX)
    }

    pub fn error(&self) -> Error {
        match self.inner {
            Ok(_) => Error::None,
            Err(e) => e,
        }
    }
}

pub trait MessageHandler: Sized + 'static {
    type ProgMsg: Send + Sync + 'static;
    type FreeMsg: Send + Sync + 'static;

    fn handle(
        &mut self,
        suspender: &mut Suspender<Self>,
        sender: Pid,
        message: Self::ProgMsg,
    ) -> HandlerResult;

    fn handle_free(&mut self, suspender: &mut Suspender<Self>, message: Self::FreeMsg);
}

#[derive(Debug)]
enum KernelProcessState {
    Suspended {
        state: ProcessorState,
        sender: Option<Pid>,
    },
    SuspendedReceiving {
        state: ProcessorState,
        sender: Option<Pid>,
    },
    Running,
    Receiving,
}

pub struct KernelProcess<T: MessageHandler> {
    handler: UnsafeCell<T>,
    prog_messages: Queue<ProgMsg<T>>,
    free_messages: Queue<FreeMsg<T>>,
    state: KernelProcessState,
    stack_top: u64,
}

impl<T: MessageHandler> KernelProcess<T> {
    pub const fn new(handler: T, stack_top: u64) -> Self {
        KernelProcess {
            handler: UnsafeCell::new(handler),
            prog_messages: Queue::new(),
            free_messages: Queue::new(),
            state: KernelProcessState::Receiving,
            stack_top,
        }
    }

    pub fn send_from_program(
        mut self: MutexGuard<Self>,
        me: &'static Mutex<Self>,
        sender: Pid,
        message: T::ProgMsg,
        data: &'static mut [u8],
    ) -> ! {
        let message = ProgMsg {
            payload: message,
            data,
            sender,
        };
        self.prog_messages.push(message);
        self.check_inbox(me);
    }

    fn pop_inbox(&mut self) -> Option<Message<T>> {
        if let Some(msg) = self.free_messages.pop() {
            Some(Message::Free(msg))
        } else if let Some(msg) = self.prog_messages.pop() {
            Some(Message::Program(msg))
        } else {
            None
        }
    }

    fn check_inbox(mut self: MutexGuard<Self>, me: &'static Mutex<Self>) -> ! {
        match self.state {
            KernelProcessState::Receiving => {}
            KernelProcessState::SuspendedReceiving { .. } => {
                if self.free_messages.len() > 0 {
                    ::core::mem::drop(self);
                    me.wake();
                } else {
                    ::core::mem::drop(self);
                    ::scheduler::lock().schedule();
                }
            }
            KernelProcessState::Suspended { .. } => {
                ::core::mem::drop(self);
                ::scheduler::lock().schedule();
            }
            KernelProcessState::Running => {
                panic!("cannot check inbox when running");
            }
        }
        if let Some(message) = self.pop_inbox() {
            if let Message::Program(ProgMsg { sender, .. }) = message {
                ::scheduler::lock().switch_to_program(sender, false);
            }
            self.state = KernelProcessState::Running;
            let params = Params {
                message,
                handler: self.handler.get(),
                process: me,
            };
            let entry_point = kernel_process_start::<T> as usize as u64;
            let params = &params as *const _ as usize as u64;
            let stack = self.stack_top;
            ::core::mem::drop(self);
            unsafe {
                kernel_process_trampoline(params, entry_point, stack);
            }
        } else {
            ::core::mem::drop(self);
            ::scheduler::lock().schedule();
        }
    }

    pub fn lock_and_send(
        this: &'static Mutex<Self>,
        sender: Pid,
        message: T::ProgMsg,
        data: &'static mut [u8],
    ) -> ! {
        this.lock().send_from_program(this, sender, message, data);
    }

    pub fn send_delayed(this: &'static Mutex<Self>, message: T::FreeMsg)
    where
        T: Send + Sync,
    {
        this.lock().free_messages.push(FreeMsg { payload: message });
        ::scheduler::lock().schedule_waker(this);
    }

    pub fn send_delayed_no_schedule(this: &'static Mutex<Self>, message: T::FreeMsg)
    where
        T: Send + Sync,
    {
        this.lock().free_messages.push(FreeMsg { payload: message });
    }
}

pub trait Waker {
    fn wake(&'static self) -> !;
    fn can_receive(&'static self) -> bool;
}

impl<T: MessageHandler> Waker for Mutex<KernelProcess<T>> {
    fn wake(&'static self) -> ! {
        use core::mem;
        let mut process = self.lock();
        let (state, pid) = match mem::replace(&mut process.state, KernelProcessState::Running) {
            KernelProcessState::Suspended { state, sender } => (state, sender),
            KernelProcessState::SuspendedReceiving { state, sender } => (state, sender),
            s @ KernelProcessState::Receiving => {
                process.state = s;
                process.check_inbox(self);
            }
            KernelProcessState::Running => panic!("cannot resume running process"),
        };
        if let Some(pid) = pid {
            ::scheduler::lock().switch_to_program(pid, false);
        }
        mem::drop(process);
        unsafe { state.kernel_jump() };
    }

    fn can_receive(&'static self) -> bool {
        let process = self.lock();
        match process.state {
            KernelProcessState::Suspended { .. } => false,
            KernelProcessState::SuspendedReceiving { .. } => process.free_messages.len() > 0,
            KernelProcessState::Receiving => {
                process.free_messages.len() > 0 || process.prog_messages.len() > 0
            }
            KernelProcessState::Running => false,
        }
    }
}

pub struct Suspender<T: MessageHandler> {
    pid: Option<Pid>,
    data: &'static mut [u8],
    process: &'static Mutex<KernelProcess<T>>,
}

impl<T: MessageHandler> Suspender<T> {
    pub fn data_mut(&mut self) -> &mut [u8] {
        self.data
    }

    pub fn data(&self) -> &[u8] {
        self.data
    }

    pub fn suspend(&mut self) {
        let mut process = self.process.lock();
        process.state = KernelProcessState::Suspended {
            state: unsafe { ProcessorState::program_initial() },
            sender: self.pid,
        };
        let state = match process.state {
            KernelProcessState::Suspended { ref mut state, .. } => state as *mut _,
            _ => unreachable!(),
        };
        ::core::mem::drop(process);
        unsafe {
            kernel_process_suspend(state);
        }
    }

    pub fn receive_free(&mut self) -> T::FreeMsg {
        let mut process = self.process.lock();
        if let Some(msg) = process.free_messages.pop() {
            ::core::mem::drop(process);
            return msg.payload;
        }
        process.state = KernelProcessState::SuspendedReceiving {
            state: unsafe { ProcessorState::program_initial() },
            sender: self.pid,
        };
        let state = match process.state {
            KernelProcessState::SuspendedReceiving { ref mut state, .. } => state as *mut _,
            _ => unreachable!(),
        };
        ::core::mem::drop(process);
        unsafe {
            kernel_process_suspend(state);
        }
        let mut process = self.process.lock();
        let msg = process
            .free_messages
            .pop()
            .expect("expected free message after waking");
        msg.payload
    }

    pub unsafe fn handle_program(&mut self, pid: Pid) {
        self.pid = Some(pid);
    }
}

struct Params<T: MessageHandler> {
    message: Message<T>,
    handler: *mut T,
    process: &'static Mutex<KernelProcess<T>>,
}

extern "C" fn kernel_process_start<T: MessageHandler>(params: &Params<T>) -> ! {
    let params = unsafe { ::core::ptr::read(params) };
    match params.message {
        Message::Program(ProgMsg {
            sender,
            data,
            payload,
        }) => {
            let mut suspender = Suspender {
                pid: Some(sender),
                data,
                process: params.process,
            };
            let handler = unsafe { &mut *params.handler };
            let result = handler.handle(&mut suspender, sender, payload);
            let mut scheduler = ::scheduler::lock();
            scheduler.write_syscall_result(sender, result);
            scheduler.set_program_state(sender, ::scheduler::NewState::Ready);
        }
        Message::Free(FreeMsg { payload }) => {
            let mut suspender = Suspender {
                pid: None,
                data: &mut [],
                process: params.process,
            };
            let handler = unsafe { &mut *params.handler };
            handler.handle_free(&mut suspender, payload);
        }
    };
    params.process.lock().state = KernelProcessState::Receiving;
    unsafe {
        kernel_process_end(
            params.process as *const _ as usize as u64,
            kernel_process_post_end::<T> as usize as u64,
        );
    }
}

#[no_mangle]
pub extern "C" fn kernel_process_post_suspend() -> ! {
    ::scheduler::lock().schedule();
}

extern "C" fn kernel_process_post_end<T: MessageHandler>(
    process: &'static Mutex<KernelProcess<T>>,
) -> ! {
    process.lock().check_inbox(process);
}

extern "C" {
    fn kernel_process_trampoline(params: u64, start: u64, stack: u64) -> !;
    fn kernel_process_suspend(state: *mut ProcessorState);
    fn kernel_process_end(param: u64, start: u64) -> !;
}
