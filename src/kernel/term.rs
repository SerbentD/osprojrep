use super::{HandlerResult, KernelProcess, MessageHandler, Suspender};
use collections::Queue;
use core::fmt::{self, Write};
use core::sync::atomic::{AtomicUsize, Ordering};
use keyboard_driver::{self, Event};
use program::Pid;
use spin::Mutex;
use vga_buffer::{self, Color, ColorCode, ScreenChar};

const BUFFER_WIDTH: usize = vga_buffer::BUFFER_WIDTH;
const BUFFER_HEIGHT: usize = vga_buffer::BUFFER_HEIGHT - 1;

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
enum TermState {
    Uninitialized,
    Shell,
    Program(Pid),
    WaitingForRun,
}

pub struct Terminal {
    index: usize,
    output: [[ScreenChar; BUFFER_WIDTH]; BUFFER_HEIGHT],
    writer_row: usize,
    writer_col: usize,
    stdin_buf: Queue<u8>,
    editor: Editor,
    state: TermState,
    force_end_read: bool,
}

impl Terminal {
    pub const fn new(index: usize) -> Self {
        Terminal {
            output: [[ScreenChar {
                ascii_character: b' ',
                color_code: ColorCode::new(Color::White, Color::Black),
            }; BUFFER_WIDTH]; BUFFER_HEIGHT],
            stdin_buf: Queue::new(),
            editor: Editor::new(),
            writer_row: 0,
            writer_col: 0,
            index,
            state: TermState::Uninitialized,
            force_end_read: false,
        }
    }

    fn write_stdout(&mut self, buf: &[u8]) {
        if self.writer_col == 0 {
            let row = self.writer_row;
            self.clear_row(row);
        }
        for &byte in buf {
            self.write_stdout_byte(byte);
        }
    }

    fn clear_row(&mut self, row: usize) {
        for col in 0..BUFFER_WIDTH {
            self.output[row][col] = ScreenChar {
                ascii_character: b' ',
                color_code: ColorCode::new(Color::White, Color::Black),
            };
        }
    }

    fn push_up(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let ch = self.output[row][col];
                self.output[row - 1][col] = ch;
            }
        }
    }

    fn write_stdout_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.stdout_newline(),
            other => {
                if self.writer_row == BUFFER_HEIGHT - 1 {
                    self.clear_row(BUFFER_HEIGHT - 1);
                    self.push_up();
                    self.writer_row -= 1;
                }
                if self.writer_col == BUFFER_WIDTH {
                    self.stdout_newline();
                }
                let row = self.writer_row;
                let col = self.writer_col;
                let color = ColorCode::new(Color::White, Color::Black);
                self.output[row][col] = ScreenChar {
                    ascii_character: other,
                    color_code: color,
                };
                self.writer_col += 1;
            }
        }
    }

    fn stdout_newline(&mut self) {
        if self.writer_col == 0 {
            let row = self.writer_row;
            self.clear_row(row);
        }
        if self.writer_row < BUFFER_HEIGHT - 1 {
            self.writer_row += 1;
        } else {
            self.push_up();
        }
        self.writer_col = 0;
    }

    fn render(&mut self) {
        if is_term_active(self.index) {
            self.rerender_editor();
            vga_buffer::WRITER
                .lock()
                .render_raw(self.index, &self.output);
        }
    }

    fn rerender_editor(&mut self) {
        let editor_line = if self.writer_col == 0 {
            self.writer_row
        } else {
            self.writer_row + 1
        };
        for i in 0..BUFFER_WIDTH {
            self.output[editor_line][i] = if i < self.editor.prefix {
                ScreenChar {
                    ascii_character: self.editor.buf[i],
                    color_code: ColorCode::new(Color::LightBlue, Color::Black),
                }
            } else if i < self.editor.cursor {
                ScreenChar {
                    ascii_character: self.editor.buf[i],
                    color_code: ColorCode::new(Color::White, Color::Black),
                }
            } else if i == self.editor.cursor {
                ScreenChar {
                    ascii_character: b' ',
                    color_code: ColorCode::new(Color::White, Color::White),
                }
            } else {
                ScreenChar {
                    ascii_character: b' ',
                    color_code: ColorCode::new(Color::White, Color::Black),
                }
            }
        }
    }

    fn go_to_shell(&mut self) {
        self.editor.init(b"/$ ");
        self.state = TermState::Shell;
    }

    fn initialize(&mut self) {
        assert_eq!(self.state, TermState::Uninitialized);
        self.go_to_shell();
    }

    fn write_editor_line(&mut self) {
        let editor_line = if self.writer_col == 0 {
            self.writer_row
        } else {
            self.writer_row + 1
        };
        let char_col = self.editor.cursor;
        if char_col < BUFFER_WIDTH {
            self.output[editor_line][char_col] = ScreenChar {
                ascii_character: b' ',
                color_code: ColorCode::new(Color::White, Color::Black),
            };
        }
        if editor_line == BUFFER_HEIGHT - 1 {
            self.push_up();
            self.writer_row = editor_line;
        } else {
            self.writer_row = editor_line + 1;
        }
        self.writer_col = 0;
    }

    fn run_program(&mut self) {
        self.force_end_read = false;
        self.write_editor_line();
        let (path, arg) = {
            let (command, arg) = if let Some(x) = split_command(self.editor.current_input()) {
                x
            } else {
                return;
            };
            let path = ::spark_fs::Path::from_ascii_str(command);
            let arg = ::program::ProgramParam::from_slice(arg);
            (path, arg)
        };
        match (path, arg) {
            (Some(path), Some(arg)) => {
                ::kernel::KernelProcess::send_delayed(
                    &::kernel::FILE_SYSTEM,
                    ::kernel::fs::FreeMsg::Run {
                        path,
                        arg,
                        terminal: self.index,
                        detach: false,
                    },
                );
                self.state = TermState::WaitingForRun;
                self.editor.init(b"");
            }
            (Some(_), None) => {
                self.write_stdout(b"cannot start: argument too long");
                self.go_to_shell();
            }
            (None, _) => {
                self.write_stdout(b"cannot start: invalid path");
                self.go_to_shell();
            }
        }
    }

    fn enter_text(&mut self) {
        match self.state {
            TermState::Uninitialized => unreachable!(),
            TermState::Shell if self.editor.current_input() == b"stat" => {
                self.write_editor_line();
                self.print_stats();
                self.go_to_shell();
            }
            TermState::Shell => {
                self.run_program();
            }
            TermState::Program(_) => {
                self.write_editor_line();
                for &ch in self.editor.current_input() {
                    if self.stdin_buf.len() == ::collections::CAPACITY {
                        break;
                    }
                    self.stdin_buf.push(ch);
                }
                if self.stdin_buf.len() < ::collections::CAPACITY {
                    self.stdin_buf.push(10);
                }
                self.editor.init(b"");
            }
            TermState::WaitingForRun => {}
        }
    }

    fn clear_screen(&mut self) {
        for row in 0..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                self.output[row][col] = ScreenChar {
                    ascii_character: b' ',
                    color_code: ColorCode::new(Color::White, Color::Black),
                };
            }
        }
        self.writer_row = 0;
        self.writer_col = 0;
        self.render();
    }

    fn handle_editor(&mut self, command: EditorCommand) {
        match command {
            EditorCommand::None => {}
            EditorCommand::Enter => {
                self.enter_text();
            }
            EditorCommand::ClearScreen => {
                self.clear_screen();
            }
        }
    }

    fn print_stats(&mut self) {
        let mut writer = self.get_formatter();
        ::scheduler::lock().write_stats(&mut writer);
    }

    fn get_formatter<'a>(&'a mut self) -> impl fmt::Write + 'a {
        struct Writer<'a> {
            term: &'a mut Terminal,
        }
        impl<'a> ::core::fmt::Write for Writer<'a> {
            fn write_str(&mut self, s: &str) -> fmt::Result {
                self.term.write_stdout(s.as_bytes());
                Ok(())
            }
        }
        Writer { term: self }
    }

    fn read_stdin(&mut self, suspender: &mut Suspender<Self>) -> usize {
        let mut read = 0;
        loop {
            let remaining = suspender.data().len() - read;
            let step = ::core::cmp::min(remaining, self.stdin_buf.len());
            for _ in 0..step {
                let byte = self.stdin_buf.pop().expect("expected to get a byte");
                suspender.data_mut()[read] = byte;
                read += 1;
            }
            if read > 0 || self.force_end_read {
                // panic!("given {} bytes", read);
                return read;
            }
            if step < remaining {
                let message = suspender.receive_free();
                self.handle_free_internal(message);
            }
        }
    }

    fn handle_free_internal(&mut self, message: FreeMsg) {
        match message {
            FreeMsg::Key(event) => {
                if event.key == ::keyboard_driver::Key::Character(::keyboard_driver::Character::C)
                    && event.control
                {
                    if let TermState::Program(pid) = self.state {
                        ::scheduler::lock().kill_program(pid, 123);
                        self.force_end_read = true;
                    }
                } else if self.state != TermState::WaitingForRun {
                    let editor_result = self.editor.handle_event(event);
                    self.handle_editor(editor_result);
                    self.render();
                }
            }
            FreeMsg::MakeActive => {
                self.render();
            }
            FreeMsg::RunResult(Ok(pid)) => {
                ::scheduler::lock().set_program_state(pid, ::scheduler::NewState::Ready);
                self.state = TermState::Program(pid);
            }
            FreeMsg::RunResult(Err(error)) => {
                self.write_stdout(b"Failed to start program\n");
                let _ = writeln!(self.get_formatter(), "{}", error);
                self.go_to_shell();
                self.render();
            }
            FreeMsg::ProgramExit(code) => {
                if code != 0 {
                    let _ = writeln!(self.get_formatter(), "\nProgram exited with code: {}", code);
                }
                self.go_to_shell();
                self.render();
            }
            FreeMsg::TraceState(pid, state) => {
                self.write_stdout_byte(b'0' + pid.0 as u8);
                self.write_stdout_byte(b' ');
                self.write_stdout(state.as_bytes());
                self.write_stdout_byte(b'\n');
            }
        }
    }
}

#[derive(Clone)]
struct Editor {
    buf: [u8; 80],
    prefix: usize,
    cursor: usize,
}

impl Editor {
    const fn new() -> Self {
        Editor {
            buf: [0; BUFFER_WIDTH],
            prefix: 0,
            cursor: 0,
        }
    }

    fn init(&mut self, prefix: &[u8]) {
        assert!(prefix.len() < self.buf.len(), "prefix too long");
        for i in 0..(self.buf.len()) {
            self.buf[i] = prefix.get(i).cloned().unwrap_or(0);
        }
        self.prefix = prefix.len();
        self.cursor = self.prefix;
    }

    fn handle_event(&mut self, event: Event) -> EditorCommand {
        match event.key {
            keyboard_driver::Key::Backspace => {
                self.cursor = self.cursor.saturating_sub(1);
                if self.cursor < self.prefix {
                    self.cursor = self.prefix;
                }
                EditorCommand::None
            }
            keyboard_driver::Key::Character(keyboard_driver::Character::L) if event.control => {
                EditorCommand::ClearScreen
            }
            keyboard_driver::Key::Enter => EditorCommand::Enter,
            _ => {
                if !event.control && self.cursor < self.buf.len() {
                    if let Some(ch) = event.to_char() {
                        self.buf[self.cursor] = ch;
                        self.cursor += 1;
                    }
                }
                EditorCommand::None
            }
        }
    }

    fn current_input(&self) -> &[u8] {
        &self.buf[self.prefix..self.cursor]
    }
}

enum EditorCommand {
    None,
    Enter,
    ClearScreen,
}

pub enum ProgMsg {
    Read,
    Write,
}

pub enum FreeMsg {
    Key(Event),
    MakeActive,
    RunResult(::spark_fs::io::Result<::program::Pid>),
    ProgramExit(u64),
    TraceState(Pid, &'static str),
}

impl MessageHandler for Terminal {
    type ProgMsg = ProgMsg;
    type FreeMsg = FreeMsg;

    fn handle(
        &mut self,
        suspender: &mut Suspender<Self>,
        _sender: Pid,
        message: ProgMsg,
    ) -> HandlerResult {
        if self.state == TermState::Uninitialized {
            self.initialize();
        }
        match message {
            ProgMsg::Write => {
                self.write_stdout(suspender.data());
                self.render();
                HandlerResult::success(suspender.data().len() as u64)
            }
            ProgMsg::Read => {
                let amount = self.read_stdin(suspender);
                HandlerResult::success(amount as u64)
            }
        }
    }

    fn handle_free(&mut self, _suspender: &mut Suspender<Self>, message: FreeMsg) {
        if self.state == TermState::Uninitialized {
            self.initialize();
        }
        self.handle_free_internal(message);
    }
}

impl keyboard_driver::Handler for Mutex<KernelProcess<Terminal>> {
    fn event(&'static self, event: Event) -> ! {
        match event.key {
            keyboard_driver::Key::F(index) => {
                assert!(1 <= index && index <= 12, "f key out of bounds");
                set_active_terminal(index as usize - 1);
            }
            _ => {
                ::kernel::KernelProcess::send_delayed(self, FreeMsg::Key(event));
            }
        }
        ::scheduler::lock().schedule();
    }
}

static ACTIVE_TERM_INDEX: AtomicUsize = AtomicUsize::new(12);

fn is_term_active(index: usize) -> bool {
    ACTIVE_TERM_INDEX.load(Ordering::SeqCst) == index
}

pub fn set_active_terminal(index: usize) {
    ACTIVE_TERM_INDEX.store(index, Ordering::SeqCst);
    keyboard_driver::KEYBOARD_DRIVER
        .lock()
        .register_handler(&TERMINALS[index]);
    ::kernel::KernelProcess::send_delayed(&TERMINALS[index], FreeMsg::MakeActive);
}

pub const fn make_terminal(index: usize, stack: u64) -> Mutex<KernelProcess<Terminal>> {
    Mutex::new(KernelProcess::new(Terminal::new(index), stack))
}

pub const STACKS: [u64; 12] = [
    1024 * 1024 * 202,
    1024 * 1024 * 203,
    1024 * 1024 * 204,
    1024 * 1024 * 205,
    1024 * 1024 * 206,
    1024 * 1024 * 207,
    1024 * 1024 * 208,
    1024 * 1024 * 209,
    1024 * 1024 * 210,
    1024 * 1024 * 211,
    1024 * 1024 * 212,
    1024 * 1024 * 213,
];

pub static TERMINALS: [Mutex<KernelProcess<Terminal>>; 12] = [
    make_terminal(0, STACKS[0]),
    make_terminal(1, STACKS[1]),
    make_terminal(2, STACKS[2]),
    make_terminal(3, STACKS[3]),
    make_terminal(4, STACKS[4]),
    make_terminal(5, STACKS[5]),
    make_terminal(6, STACKS[6]),
    make_terminal(7, STACKS[7]),
    make_terminal(8, STACKS[8]),
    make_terminal(9, STACKS[9]),
    make_terminal(10, STACKS[10]),
    make_terminal(11, STACKS[11]),
];

fn split_command(command: &[u8]) -> Option<(&[u8], &[u8])> {
    let mut first = 0;
    while command.get(first) == Some(&b' ') {
        first += 1;
    }
    if first == command.len() {
        return None;
    }
    let mut space = first;
    while space < command.len() && command[space] != b' ' {
        space += 1;
    }
    let com = &command[first..space];
    let arg = trim(&command[space..]);
    Some((com, arg))
}

fn trim(text: &[u8]) -> &[u8] {
    let mut first = 0;
    while text.get(first) == Some(&b' ') {
        first += 1;
    }
    let mut last = text.len();
    while last > first && text[last - 1] == b' ' {
        last -= 1;
    }
    &text[first..last]
}
