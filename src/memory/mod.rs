//! Utilities to manage physical and virtual memory.
//!
//! Currently the main things are:
//! * Allocators for physical memory frames (see trait [`FrameAllocator`]).
//! * Stuff for handling page tables (see
//! [`ActivePageTable`](paging::ActivePageTable),
//! [`InactivePageTable`](paging::InactivePageTable)).

pub mod frame_allocators;
// pub mod heap_allocator;
// Everything would go to shit if we used this module on other archs.
#[cfg(target_arch = "x86_64")]
pub mod paging;

use self::paging::temporary_page::TemporaryPage;
use multiboot2::BootInformation;

/// A frame represents a physical memory frame of size [`PAGE_SIZE`](PAGE_SIZE).
#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct Frame {
    number: usize,
}

/// A range of physical frames.
///
/// This struct is returned by [`Frame::range_inclusive`].
pub struct FrameIter {
    from: Frame,
    to: Frame,
}

/// Page size in bytes.
pub const PAGE_SIZE: usize = 4096;

impl Frame {
    /// Get a frame that contains given physical address.
    ///
    /// This function is not public, because outside this module we are not
    /// supposed to be freely creating and destroying frames - the only correct
    /// way to handle frames is through [`FrameAllocator`](FrameAllocator).
    fn containing_address(address: usize) -> Frame {
        Frame {
            number: address / PAGE_SIZE,
        }
    }

    /// Physical address of the first byte in this frame.
    pub fn start_address(&self) -> usize {
        self.number * PAGE_SIZE
    }

    fn clone(&self) -> Frame {
        // Frame is not Clone to prevent stuff like double
        // free if used carelessly outside `memory` module.
        Frame {
            number: self.number,
        }
    }

    /// Yield all the frames in the given range.
    fn range_inclusive(from: Frame, to: Frame) -> FrameIter {
        FrameIter { from, to }
    }
}

impl Iterator for FrameIter {
    type Item = Frame;

    fn next(&mut self) -> Option<Frame> {
        if self.from <= self.to {
            let frame = self.from.clone();
            self.from.number += 1;
            Some(frame)
        } else {
            None
        }
    }
}

/// Allocates and frees physical memory frames.
pub trait FrameAllocator {
    /// Get an unused frame. Returns [`None`] if there are no free frames.
    fn allocate(&mut self) -> Option<Frame>;
    /// Return a frame to the unused frame pool.
    fn deallocate(&mut self, frame: Frame);
}

/// Initialize proper memory mapping.
///
/// # Panics
///
/// This function assumes that current mapping was created by boot assembly code,
/// and therefore will panic if called more that once.
pub fn init(boot_info: &BootInformation) -> Controller<frame_allocators::Alloc> {
    {
        // TODO: a hack to check that `init` is called at most once.
        // Should be replaced with something nicer, for example an external crate.
        use core::sync::atomic::{AtomicBool, Ordering};
        static CHECK: AtomicBool = AtomicBool::new(false);
        if CHECK.swap(true, Ordering::SeqCst) {
            panic!("memory::init was called twice");
        }
    }

    let memory_map_tag = boot_info.memory_map_tag().expect("Missing memory map tag.");
    let sections_tag = boot_info.elf_sections_tag().expect("Missing sections tag.");

    let kernel_start = sections_tag
        .sections()
        .filter(|s| s.is_allocated())
        .map(|s| s.start_address())
        .min()
        .unwrap();
    let kernel_end = sections_tag
        .sections()
        .filter(|s| s.is_allocated())
        .map(|s| s.end_address())
        .max()
        .unwrap();
    let multiboot_start = boot_info.start_address();
    let multiboot_end = boot_info.end_address();

    let mut allocator = frame_allocators::Alloc::new(
        memory_map_tag.memory_areas(),
        kernel_start as usize,
        kernel_end as usize,
        multiboot_start,
        multiboot_end,
    );

    let mut active_table = unsafe { paging::ActivePageTable::new() };
    let mut temp_page = TemporaryPage::new(
        Page {
            number: 0xCAFE_BABE,
        },
        &mut allocator,
    );
    paging::remap_kernel(
        &mut active_table,
        &mut allocator,
        &mut temp_page,
        &boot_info,
    );

    use self::paging::Page;

    {
        let mut map_process_stack = |stack_top: u64| {
            for offset in 0..10 {
                active_table.map(
                    Page::containing_address(stack_top as usize - offset * 4096 - 1),
                    paging::entry::EntryFlags::WRITABLE,
                    &mut allocator,
                );
            }
        };

        //map_process_stack(::kernel::dummy::STACK);
        map_process_stack(::kernel::fs::STACK);
        for &stack in &::kernel::term::STACKS {
            map_process_stack(stack);
        }
    }

    let temp_page = TemporaryPage::new(
        Page {
            number: 0xCAFE_BABE,
        },
        &mut allocator,
    );
    Controller {
        allocator,
        active_table,
        temp_page,
    }
}

pub struct Controller<A> {
    pub allocator: A,
    pub active_table: paging::ActivePageTable,
    pub temp_page: TemporaryPage,
}
