//! A very simple frame allocator.
//!
//! This allocator simply allocates all physical frames in their physical
//! address order. It does not free frames - all freed frames are simply leaked.

use memory::{Frame, FrameAllocator, PAGE_SIZE};
use multiboot2::MemoryAreaIter;

/// A leaking allocator.
pub struct LeakingAllocator {
    current: Option<(usize, usize)>,
    areas: MemoryAreaIter,
    restricted: [(usize, usize); 2],
}

impl LeakingAllocator {
    /// Create a new allocator.
    ///
    /// This allocator simply used an iterator over the elf sections (that is
    /// provided by multiboot2 crate) to know what areas of physical memory can
    /// be used. Kernel and multiboot info addressed are required to know which
    /// pages must be left unallocated. Both ranges are inclusive at the start
    /// and exclusive at the end.
    pub fn new(
        areas: MemoryAreaIter,
        kernel_start: usize,
        kernel_end: usize,
        multiboot_start: usize,
        multiboot_end: usize,
    ) -> Self {
        LeakingAllocator {
            current: None,
            areas,
            restricted: [(kernel_start, kernel_end), (multiboot_start, multiboot_end)],
        }
    }

    /// Check if frame can be allocated.
    ///
    /// Frames that intersect kernel or multiboot info must not be allocated,
    /// as they cannot be modified without taking extreme care.
    fn is_frame_bad(&self, frame: &Frame) -> bool {
        self.restricted
            .iter()
            .any(|&(s, e)| intersects(frame, s, e))
    }
}

/// Check if frame has any common bytes with given address range. The range is
/// inclusive at the start, and exclusive at the end.
fn intersects(frame: &Frame, start: usize, end: usize) -> bool {
    let frame_start = frame.number * PAGE_SIZE;
    let frame_end = frame_start + PAGE_SIZE;
    frame_end > start && frame_start < end
}

impl FrameAllocator for LeakingAllocator {
    fn allocate(&mut self) -> Option<Frame> {
        loop {
            if let Some((start, size)) = self.current {
                let frame = Frame::containing_address(start);
                let new_start = start + PAGE_SIZE;
                let new_size = size - PAGE_SIZE;
                if new_size == 0 {
                    self.current = None;
                } else {
                    self.current = Some((new_start, new_size));
                }
                if !self.is_frame_bad(&frame) {
                    return Some(frame);
                }
            } else if let Some(area) = self.areas.next() {
                let start = area.start_address();
                // start should be rounded up to next page start
                let rounded_start = (start + PAGE_SIZE - 1) / PAGE_SIZE * PAGE_SIZE;
                let dropped = rounded_start - start;
                // also round size down to page size multiple
                let size = (area.size() - dropped) / PAGE_SIZE * PAGE_SIZE;
                self.current = Some((rounded_start, size));
            } else {
                return None;
            }
        }
    }

    fn deallocate(&mut self, _frame: Frame) {
        // dummy allocator, so no frame reusing
    }
}
