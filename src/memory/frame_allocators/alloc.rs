use memory::frame_allocators::LeakingAllocator;
use memory::{Frame, FrameAllocator, PAGE_SIZE};
use multiboot2::MemoryAreaIter;

const FREE_STACK_SIZE: usize = 16 * 1024 * 1024 * 1024 / PAGE_SIZE;
static mut FREE_STACK: [u32; FREE_STACK_SIZE] = [0; FREE_STACK_SIZE];

pub struct MemoryStats {
    pub used: u64,
    pub total: u64,
}

pub struct Alloc {
    free: usize,
    total: usize,
}

impl Alloc {
    /// Create a new allocator.
    ///
    /// # Panics
    ///
    /// Because of a hack in its implementation, only one instance
    /// of the allocator can exist at a time. Conservatively, this
    /// function will panic if called a second time.
    pub fn new(
        areas: MemoryAreaIter,
        kernel_start: usize,
        kernel_end: usize,
        multiboot_start: usize,
        multiboot_end: usize,
    ) -> Self {
        {
            // TODO: this is a hack
            use core::sync::atomic::{AtomicBool, Ordering};
            static CHECK: AtomicBool = AtomicBool::new(false);
            if CHECK.swap(true, Ordering::SeqCst) {
                panic!("Alloc::new was called twice");
            }
        }
        let mut leaking_alloc = LeakingAllocator::new(
            areas,
            kernel_start,
            kernel_end,
            multiboot_start,
            multiboot_end,
        );
        let mut free = 0;
        while let Some(frame) = leaking_alloc.allocate() {
            if frame.number > ::core::u32::MAX as usize {
                continue;
            }
            if free == FREE_STACK_SIZE {
                break;
            }
            unsafe {
                FREE_STACK[free] = frame.number as u32;
            }
            free += 1;
        }
        Alloc { free, total: free }
    }

    pub fn memory_stats(&self) -> MemoryStats {
        let total = (self.total * PAGE_SIZE) as u64;
        let used = ((self.total - self.free) * PAGE_SIZE) as u64;
        MemoryStats { total, used }
    }
}

impl FrameAllocator for Alloc {
    fn allocate(&mut self) -> Option<Frame> {
        if self.free > 0 {
            self.free -= 1;
            let index = unsafe { FREE_STACK[self.free] };
            Some(Frame {
                number: index as usize,
            })
        } else {
            None
        }
    }

    fn deallocate(&mut self, frame: Frame) {
        let num = frame.number as u32;
        unsafe {
            FREE_STACK[self.free] = num;
        }
        self.free += 1;
    }
}
