//! A collection of physical frame allocators.

mod alloc;
mod leaking_allocator;
mod tiny_allocator;

pub use self::alloc::Alloc;
pub use self::leaking_allocator::LeakingAllocator;
pub use self::tiny_allocator::TinyAllocator;
