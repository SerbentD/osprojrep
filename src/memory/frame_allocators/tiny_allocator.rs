//! A small allocator for use with
//! [`TemporaryPage`](super::paging::temporary_page::TemporaryPage).

use memory::{Frame, FrameAllocator};

/// An allocator for use with
/// [`TemporaryPage`](super::paging::temporary_page::TemporaryPage).
///
/// This allocator holds 3 pages that will be used if intermediate page table
/// levels are not yet created.
pub struct TinyAllocator([Option<Frame>; 3]);

impl TinyAllocator {
    /// Create a new TinyAllocator.
    ///
    /// This allocator will fill up its internal buffer with the given
    /// allocator.
    pub fn new<A: FrameAllocator>(allocator: &mut A) -> TinyAllocator {
        TinyAllocator([
            allocator.allocate(),
            allocator.allocate(),
            allocator.allocate(),
        ])
    }

    pub fn fill_up<A: FrameAllocator>(&mut self, allocator: &mut A) {
        for place in &mut self.0 {
            if place.is_none() {
                *place = allocator.allocate();
            }
        }
    }
}

impl FrameAllocator for TinyAllocator {
    fn allocate(&mut self) -> Option<Frame> {
        for frame in &mut self.0 {
            if frame.is_some() {
                return frame.take();
            }
        }
        None
    }

    fn deallocate(&mut self, frame: Frame) {
        for place in &mut self.0 {
            if place.is_none() {
                *place = Some(frame);
                return;
            }
        }
        panic!("TinyAllocator can't hold any more frames");
    }
}
