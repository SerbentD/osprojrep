//! Utilities for representing and navigating page tables.

use core::marker::PhantomData;
use core::ops::{Index, IndexMut};
use memory::paging::entry::{Entry, EntryFlags};
use memory::paging::ENTRY_COUNT;
use memory::FrameAllocator;

/// Address of currently active p4 table.
///
/// Because we are using recursive mapping technique, where the last entry of p4
/// table points to itself, this table will always be accessible at this fixed
/// virtual address.
pub const P4: *mut Table<Level4> = 0xffffffff_fffff000 as *mut _;

/// Marker trait for types that are used to mark page table level.
pub trait TableLevel {}

/// Marker type for safe navigation through page tables.
pub enum Level1 {}

/// Marker type for safe navigation through page tables.
pub enum Level2 {}

/// Marker type for safe navigation through page tables.
pub enum Level3 {}

/// Marker type for safe navigation through page tables.
pub enum Level4 {}

impl TableLevel for Level1 {}
impl TableLevel for Level2 {}
impl TableLevel for Level3 {}
impl TableLevel for Level4 {}

/// Helper trait for safe navigation through page tables.
pub trait HierarchicalLevel: TableLevel {
    /// Marker type for lower page table level.
    type NextLevel: TableLevel;

    fn numeric_level() -> u8;
}

impl HierarchicalLevel for Level4 {
    type NextLevel = Level3;

    fn numeric_level() -> u8 {
        4
    }
}
impl HierarchicalLevel for Level3 {
    type NextLevel = Level2;

    fn numeric_level() -> u8 {
        3
    }
}
impl HierarchicalLevel for Level2 {
    type NextLevel = Level1;

    fn numeric_level() -> u8 {
        2
    }
}

/// A single level of the page table.
pub struct Table<L: TableLevel> {
    entries: [Entry; ENTRY_COUNT],
    marker: PhantomData<L>,
}

impl<L: TableLevel> Index<usize> for Table<L> {
    type Output = Entry;

    fn index(&self, index: usize) -> &Entry {
        &self.entries[index]
    }
}

impl<L: TableLevel> IndexMut<usize> for Table<L> {
    fn index_mut(&mut self, index: usize) -> &mut Entry {
        &mut self.entries[index]
    }
}

impl<L: TableLevel> Table<L> {
    /// Set all entries to unused.
    pub fn zero(&mut self) {
        for entry in self.entries.iter_mut() {
            entry.set_unused();
        }
    }

    /// TODO: make it not-a-hack
    pub unsafe fn kernel_copy_hack(&mut self) -> &mut [Entry] {
        &mut self.entries[..128]
    }
}

impl Table<Level4> {
    pub unsafe fn destroy_program_table<A: FrameAllocator>(&mut self, alloc: &mut A) {
        // last entry is the recursive mapping - we don't want to free that yet
        for index in 0..(ENTRY_COUNT - 1) {
            if self.entries[index].is_unused() {
                continue;
            }
            {
                let next = self.next_table_mut(index)
                    .expect("entry used, but no next table");
                next.inner_destroy(alloc, index == 0);
            }
            let frame = self.entries[index]
                .pointed_frame()
                .expect("entry contains table but not frame");
            alloc.deallocate(frame);
        }
    }
}

impl Table<Level3> {
    unsafe fn inner_destroy<A: FrameAllocator>(&mut self, alloc: &mut A, has_kernel: bool) {
        for index in 0..ENTRY_COUNT {
            if self.entries[index].is_unused() {
                continue;
            }
            {
                let next = self.next_table_mut(index)
                    .expect("entry used, but no next table");
                next.inner_destroy(alloc, index == 0 && has_kernel);
            }
            let frame = self.entries[index]
                .pointed_frame()
                .expect("entry contains table but not frame");
            alloc.deallocate(frame);
        }
    }
}

impl Table<Level2> {
    unsafe fn inner_destroy<A: FrameAllocator>(&mut self, alloc: &mut A, has_kernel: bool) {
        for index in 0..ENTRY_COUNT {
            if self.entries[index].is_unused() {
                continue;
            }
            if has_kernel && index < 128 {
                continue;
            }
            {
                let next = self.next_table_mut(index)
                    .expect("entry used, but no next table");
                next.inner_destroy(alloc);
            }
            let frame = self.entries[index]
                .pointed_frame()
                .expect("entry contains table but not frame");
            alloc.deallocate(frame);
        }
    }
}

impl Table<Level1> {
    unsafe fn inner_destroy<A: FrameAllocator>(&mut self, alloc: &mut A) {
        for entry in self.entries.iter_mut() {
            if entry.is_unused() {
                continue;
            }
            let frame = entry
                .pointed_frame()
                .expect("entry used but does not contain a frame");
            alloc.deallocate(frame);
        }
    }
}

impl<L: HierarchicalLevel> Table<L> {
    /// Get physical address of the frame pointed to by the entry with given
    /// index.
    fn next_table_address(&self, index: usize) -> Option<usize> {
        use super::entry::EntryFlags as Flags;
        let entry_flags = self.entries[index].flags();
        if entry_flags.contains(Flags::PRESENT) && !entry_flags.contains(Flags::HUGE_PAGE) {
            let address = self as *const Table<L> as usize;
            Some((address << 9) | (index << 12))
        } else {
            None
        }
    }

    /// Get reference to the lower table pointed to by the entry at given index.
    pub fn next_table(&self, index: usize) -> Option<&Table<L::NextLevel>> {
        self.next_table_address(index)
            .map(|a| unsafe { &*(a as *const _) })
    }

    /// Get a mutable reference to the lower table pointed to by the entry at
    /// given index.
    pub fn next_table_mut(&mut self, index: usize) -> Option<&mut Table<L::NextLevel>> {
        self.next_table_address(index)
            .map(|a| unsafe { &mut *(a as *mut _) })
    }

    /// Get a mutable reference to the lower table pointed to by the entry at
    /// given index. The table will be created if it does not exist yet.
    pub fn next_table_create<A: FrameAllocator>(
        &mut self,
        index: usize,
        allocator: &mut A,
    ) -> &mut Table<L::NextLevel> {
        if self.next_table(index).is_none() {
            let frame = allocator.allocate().expect("no frames available");
            self.entries[index].set(
                frame,
                EntryFlags::PRESENT | EntryFlags::WRITABLE | EntryFlags::USER_ACCESSIBLE,
            );
            let table = self.next_table_mut(index).unwrap();
            table.zero();
            table
        } else {
            self.next_table_mut(index).unwrap()
        }
    }
}
