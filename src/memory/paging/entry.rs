//! Utilities for dealing with an entry in a page table.

use memory::Frame;

/// An entry in a page table.
pub struct Entry(u64);

impl Entry {
    /// Is this entry unused?
    pub fn is_unused(&self) -> bool {
        self.0 == 0
    }

    /// Clear this entry.
    ///
    /// If this entry logically owns the frame it points to, you might want to
    /// previously get the [`pointed_frame`](Entry::pointed_frame) and free it.
    pub fn set_unused(&mut self) {
        self.0 = 0;
    }

    /// Flags set for this entry.
    pub fn flags(&self) -> EntryFlags {
        EntryFlags::from_bits_truncate(self.0)
    }

    /// Returns the physical frame that this entry points to, if any. As
    /// [`Frame`s](super::super::Frame) aren't [`Clone`], you should be carefull
    /// about giving anyone this frame, as you are not supposed to get them in
    /// any other way than from [`FrameAllocator`](super::super::FrameAllocator).
    pub fn pointed_frame(&self) -> Option<Frame> {
        if self.flags().contains(EntryFlags::PRESENT) {
            Some(Frame::containing_address(
                self.0 as usize & 0x000fffff_fffff000,
            ))
        } else {
            None
        }
    }

    /// Make this entry point to the given frame and use given set of flags.
    pub fn set(&mut self, frame: Frame, flags: EntryFlags) {
        assert_eq!(
            frame.start_address() & !0x000fffff_fffff000,
            0,
            "invalid frame address"
        );
        self.0 = (frame.start_address() as u64) | flags.bits()
    }
}

bitflags! {
    /// Flags that can be set in page table entry.
    pub struct EntryFlags: u64 {
        /// Whether this page is present (mapped).
        const PRESENT = 1 << 0;
        /// Can this page be written to.
        const WRITABLE = 1 << 1;
        /// Is this page accessible in user mode.
        const USER_ACCESSIBLE = 1 << 2;
        const WRITE_THROUGH = 1 << 3;
        const NO_CACHE = 1 << 4;
        /// This flag is set by the processor when the page is accessed.
        const ACCESSED = 1 << 5;
        /// This flag is set by the processor when the page is written to.
        const DIRTY = 1 << 6;
        /// Is this mapping using a huge page, instead of the standard 4KiB one.
        const HUGE_PAGE = 1 << 7;
        const GLOBAL = 1 << 8;
        /// If the flag is set, memory in this page is not executable.
        const NO_EXECUTE = 1 << 63;
    }
}
