//! An utility for managing currently active page table.

use super::entry::{Entry, EntryFlags};
use super::table::{self, Level4, Table};
use super::{PhysAddr, VirtAddr};
use core::ptr::Unique;
use memory::paging::{Page, ENTRY_COUNT};
use memory::{Frame, FrameAllocator, PAGE_SIZE};

/// A unique handle to the active page table.
///
/// The difference between this and [`ActivePageTable`](super::ActivePageTable)
/// is that `Mapper` does not provide function `with`. Calling `with` inside
/// the closure provided to `with` would probably be a Bad Thing™, so the
/// closure gets only a `Mapper`.
pub struct Mapper {
    p4: Unique<Table<Level4>>,
}

impl Mapper {
    /// Get a handle to the active page table.
    ///
    /// # Safety
    ///
    /// `Mapper::new` has the same safety implications as
    /// [`ActivePageTable::new`](super::ActivePageTable::new).
    pub unsafe fn new() -> Mapper {
        Mapper {
            p4: Unique::new_unchecked(table::P4),
        }
    }

    /// Get a reference to the p4 table.
    pub fn p4(&self) -> &Table<Level4> {
        unsafe { self.p4.as_ref() }
    }

    /// Get a mutable reference to the p4 table.
    pub fn p4_mut(&mut self) -> &mut Table<Level4> {
        unsafe { self.p4.as_mut() }
    }

    /// Calculate a physical address that corresponds to the given virtual
    /// address.
    pub fn translate(&self, address: VirtAddr) -> Option<PhysAddr> {
        let offset = address % PAGE_SIZE;
        self.translate_page(Page::containing_address(address))
            .map(|frame| frame.number * PAGE_SIZE + offset)
    }

    /// Calculate a physical frame that corresponds to the given virtual page.
    pub fn translate_page(&self, page: Page) -> Option<Frame> {
        let p3 = self.p4().next_table(page.p4_index());

        p3.and_then(|p3| p3.next_table(page.p3_index()))
            .and_then(|p2| p2.next_table(page.p2_index()))
            .and_then(|p1| p1[page.p1_index()].pointed_frame())
            .or_else(|| {
                // try to translate this using huge page mappings
                p3.and_then(|p3| {
                    let p3_entry = &p3[page.p3_index()];
                    // check if there's a huge page in p3 table
                    if let Some(frame) = p3_entry.pointed_frame() {
                        if p3_entry.flags().contains(EntryFlags::HUGE_PAGE) {
                            // huge pages in p3 must be 1GiB aligned
                            assert_eq!(
                                frame.number % (ENTRY_COUNT * ENTRY_COUNT),
                                0,
                                "misaligned huge frame"
                            );
                            return Some(Frame {
                                number: frame.number + ENTRY_COUNT * page.p2_index()
                                    + page.p1_index(),
                            });
                        }
                    }
                    if let Some(p2) = p3.next_table(page.p3_index()) {
                        let p2_entry = &p2[page.p2_index()];
                        // check if there's a huge page in p2 table
                        if let Some(frame) = p2_entry.pointed_frame() {
                            if p2_entry.flags().contains(EntryFlags::HUGE_PAGE) {
                                // huge pages in p2 must be 2MiB aligned
                                assert_eq!(frame.number % ENTRY_COUNT, 0, "misaligned huge frame");
                                return Some(Frame {
                                    number: frame.number + page.p1_index(),
                                });
                            }
                        }
                    }
                    None
                })
            })
    }

    /// Map given physical frame at the given virtual page.
    pub fn map_to<A: FrameAllocator>(
        &mut self,
        page: Page,
        frame: Frame,
        flags: EntryFlags,
        allocator: &mut A,
    ) {
        let p4 = self.p4_mut();
        let p3 = p4.next_table_create(page.p4_index(), allocator);
        let p2 = p3.next_table_create(page.p3_index(), allocator);
        let p1 = p2.next_table_create(page.p2_index(), allocator);
        assert!(p1[page.p1_index()].is_unused());
        p1[page.p1_index()].set(frame, flags | EntryFlags::PRESENT);
    }

    /// Map some free physical frame at the given virtual page.
    pub fn map<A: FrameAllocator>(&mut self, page: Page, flags: EntryFlags, allocator: &mut A) {
        let frame = allocator.allocate().expect("out of memory");
        self.map_to(page, frame, flags, allocator);
    }

    /// Map given physical frame at the same virtual address.
    pub fn identity_map<A: FrameAllocator>(
        &mut self,
        frame: Frame,
        flags: EntryFlags,
        allocator: &mut A,
    ) {
        let page = Page::containing_address(frame.start_address());
        self.map_to(page, frame, flags, allocator);
    }

    /// Unmaps given virtual page.
    ///
    /// If a page table directory becomes empty after unmapping the page,
    /// it's frame won't be returned to the allocator. This might cause a small
    /// memory overhead which ideally would be avoided. TODO: figure out a nice
    /// way to do that.
    pub fn unmap<A: FrameAllocator>(&mut self, page: Page, allocator: &mut A) {
        let frame = self.unmap_and_return(page);
        allocator.deallocate(frame);
    }

    /// Unmaps given virtual page, and returns corresponding frame.
    ///
    /// If a page table directory becomes empty after unmapping the page,
    /// it's frame won't be returned to the allocator. This might cause a small
    /// memory overhead which ideally would be avoided. TODO: figure out a nice
    /// way to do that.
    pub fn unmap_and_return(&mut self, page: Page) -> Frame {
        assert!(self.translate(page.start_address()).is_some());
        let p1 = self.p4_mut()
            .next_table_mut(page.p4_index())
            .and_then(|p3| p3.next_table_mut(page.p3_index()))
            .and_then(|p2| p2.next_table_mut(page.p2_index()))
            .expect("cannot unmap huge page");
        let frame = p1[page.p1_index()].pointed_frame().unwrap();
        p1[page.p1_index()].set_unused();

        let addr = ::x86_64::VirtualAddress(page.start_address());
        ::x86_64::instructions::tlb::flush(addr);

        frame
    }

    pub fn get_kernel_entries(&mut self) -> &mut [Entry] {
        unsafe {
            self.p4_mut()
                .next_table_mut(0)
                .unwrap()
                .next_table_mut(0)
                .unwrap()
                .kernel_copy_hack()
        }
    }

    pub unsafe fn destroy_program_table<A: FrameAllocator>(&mut self, allocator: &mut A) {
        self.p4_mut().destroy_program_table(allocator);
    }
}
