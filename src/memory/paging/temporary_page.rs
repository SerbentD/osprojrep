//! Helper for temporarily mapping a frame at a virtual address.

use super::entry::EntryFlags;
use super::table::{Level1, Table};
use super::{ActivePageTable, Page, VirtAddr};
use memory::frame_allocators::TinyAllocator;
use memory::{Frame, FrameAllocator};

/// A page that can be temporarily mapped to some frame.
///
/// TODO: after throwing the temporary page away, temporary frames that are held
/// in its [`TinyAllocator`] will be lost. This needs to be solved somehow.
pub struct TemporaryPage {
    page: Page,
    allocator: TinyAllocator,
}

impl TemporaryPage {
    /// Create a new temporary page.vga_buffer
    ///
    /// This requires an allocator because mapping a page might require creating
    /// page tables in case they don't exist yet.
    pub fn new<A: FrameAllocator>(page: Page, allocator: &mut A) -> TemporaryPage {
        TemporaryPage {
            page,
            allocator: TinyAllocator::new(allocator),
        }
    }

    pub fn fill_up<A: FrameAllocator>(&mut self, allocator: &mut A) {
        self.allocator.fill_up(allocator);
    }

    /// Map a frame at this page.
    pub fn map(&mut self, frame: Frame, active_table: &mut ActivePageTable) -> VirtAddr {
        assert!(
            active_table.translate_page(self.page).is_none(),
            "temporary page already mapped"
        );
        active_table.map_to(self.page, frame, EntryFlags::WRITABLE, &mut self.allocator);
        self.page.start_address()
    }

    /// Map a frame at this page, and interpret the page as a page table.
    ///
    /// The returned table has lowest level, because we don't want to always
    /// allow navigating deeper.
    pub fn map_table_frame(
        &mut self,
        frame: Frame,
        active_table: &mut ActivePageTable,
    ) -> &mut Table<Level1> {
        let addr = self.map(frame, active_table);
        unsafe { &mut *(addr as *mut _) }
    }

    /// Unmap and free the mapped frame.
    pub fn unmap(&mut self, active_table: &mut ActivePageTable) -> Frame {
        active_table.unmap_and_return(self.page)
    }
}
