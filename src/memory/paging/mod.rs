//! Utilities for handling page tables.

pub mod entry;
mod mapper;
pub mod table;
pub mod temporary_page;

use self::entry::EntryFlags;
pub use self::mapper::Mapper;
pub use self::temporary_page::TemporaryPage;
use core::ops::{Deref, DerefMut};
use memory::{Controller, Frame, FrameAllocator, PAGE_SIZE};
use multiboot2::BootInformation;

/// Number of entries in a page table.
const ENTRY_COUNT: usize = 512;

/// Type alias to make function signatures clearer.
pub type PhysAddr = usize;

/// Type alias to make function signatures clearer.
pub type VirtAddr = usize;

/// A page represents a block of virtual memory of size
/// [`PAGE_SIZE`](super::PAGE_SIZE).
#[derive(Debug, Copy, Clone)]
pub struct Page {
    /// TODO: this is public because of a hack - we need this
    /// to be able to construct a temporary page in memory::init()
    pub number: usize,
}

/// A range of virtual pages.
///
/// This struct is returned by [`Page::range_inclusive`].
pub struct PageIter {
    from: Page,
    to: Page,
}

impl Page {
    /// Get a virtual page that contains given virtual address.
    ///
    /// # Panics
    ///
    /// Virtual addresses must be 48 bits long, and the top 16 bits must be sign
    /// extension of 47th bit. This function will panic if given virtual address
    /// is not valid.
    pub fn containing_address(address: VirtAddr) -> Page {
        assert!(
            address < 0x0000_8000_0000_0000 || address >= 0xffff_8000_0000_0000,
            "invalid address 0x{:x}",
            address
        );
        Page {
            number: address / PAGE_SIZE,
        }
    }

    /// Virtual address of the first byte in this page.
    pub fn start_address(&self) -> usize {
        self.number * PAGE_SIZE
    }

    /// Yield all pages in the given range.
    pub fn range_inclusive(from: Page, to: Page) -> PageIter {
        PageIter { from, to }
    }

    fn p4_index(&self) -> usize {
        (self.number >> 27) & 0o777
    }

    fn p3_index(&self) -> usize {
        (self.number >> 18) & 0o777
    }

    fn p2_index(&self) -> usize {
        (self.number >> 9) & 0o777
    }

    fn p1_index(&self) -> usize {
        self.number & 0o777
    }
}

impl Iterator for PageIter {
    type Item = Page;

    fn next(&mut self) -> Option<Page> {
        if self.from.number <= self.to.number {
            let page = self.from;
            self.from.number += 1;
            Some(page)
        } else {
            None
        }
    }
}

/// An unique handle to the currently used page table.
pub struct ActivePageTable {
    mapper: Mapper,
}

impl ActivePageTable {
    /// Create a handle to active page table.
    ///
    /// # Safety
    ///
    /// As the handle is unique, this function must not be called if there
    /// already exists a live `ActivePageTable`.
    pub unsafe fn new() -> ActivePageTable {
        ActivePageTable {
            mapper: Mapper::new(),
        }
    }

    /// Run a closure with a different active page table.
    ///
    /// This will recursively map given inactive page table, run the given
    /// closure, and then restore current table. Because inactive table is just
    /// mapped recursively instead of being properly switched to, this means
    /// that all the memory mapped by currently active table will also be mapped
    /// when inside the closure - but the mapper will modify that inactive table
    /// instead of the active one. This function is used to modify an inactive
    /// before switching to it.
    pub fn with<O, F: FnOnce(&mut Mapper) -> O>(
        &mut self,
        table: &mut InactivePageTable,
        temporary_page: &mut TemporaryPage,
        f: F,
    ) -> O {
        use x86_64::instructions::tlb;
        use x86_64::registers::control_regs;

        let result = {
            let backup = Frame::containing_address(control_regs::cr3().0 as usize);

            let p4_table = temporary_page.map_table_frame(backup.clone(), self);

            self.p4_mut()[511].set(
                table.p4_frame.clone(),
                EntryFlags::PRESENT | EntryFlags::WRITABLE,
            );
            tlb::flush_all();

            let result = f(self);

            p4_table[511].set(backup, EntryFlags::PRESENT | EntryFlags::WRITABLE);
            tlb::flush_all();

            result
        };

        temporary_page.unmap(self);

        result
    }

    /// Switch to a new page table.
    ///
    /// Currently active page table (which becomes inactive after this function)
    /// will be returned as an `InactivePageTable`.
    pub fn switch(&mut self, table: InactivePageTable) -> InactivePageTable {
        use x86_64::registers::control_regs;
        use x86_64::PhysicalAddress;

        let previous_table = InactivePageTable {
            p4_frame: Frame::containing_address(control_regs::cr3().0 as usize),
        };
        unsafe {
            control_regs::cr3_write(PhysicalAddress(table.p4_frame.start_address() as u64));
        }
        previous_table
    }
}

impl Deref for ActivePageTable {
    type Target = Mapper;

    fn deref(&self) -> &Mapper {
        &self.mapper
    }
}

impl DerefMut for ActivePageTable {
    fn deref_mut(&mut self) -> &mut Mapper {
        &mut self.mapper
    }
}

/// A page table that is not currently in use.
///
/// You can modify an inactive table with
/// [`ActivePageTable::with`](memory::paging::ActivePageTable::with), or switch
/// to using one with
/// [`ActivePageTable::switch`](memory::paging::ActivePageTable::switch).
pub struct InactivePageTable {
    p4_frame: Frame,
}

impl InactivePageTable {
    /// Create a new page table.
    ///
    /// `frame` will become the p4 entry in the page table. Currently active
    /// table and a temporary page are required for initialization of this
    /// entry.
    pub fn new<A: FrameAllocator>(
        frame: Frame,
        active_table: &mut ActivePageTable,
        temp_page: &mut TemporaryPage,
        allocator: &mut A,
    ) -> InactivePageTable {
        {
            let table = temp_page.map_table_frame(frame.clone(), active_table);
            table.zero();
            table[511].set(frame.clone(), EntryFlags::PRESENT | EntryFlags::WRITABLE);
        }
        let unmapped_frame = temp_page.unmap(active_table);
        assert_eq!(frame, unmapped_frame);
        temp_page.fill_up(allocator);
        InactivePageTable { p4_frame: frame }
    }

    pub fn destroy<A: FrameAllocator>(mut self, controller: &mut Controller<A>) {
        let Controller {
            ref mut active_table,
            ref mut allocator,
            ref mut temp_page,
        } = *controller;
        active_table.with(&mut self, temp_page, |mapper| {
            unsafe { mapper.destroy_program_table(allocator) };
        });
        allocator.deallocate(self.p4_frame);
    }
}

/// Switch to a page table that exactly matches kernel sections.
///
/// This function is supposed to be called immediately after getting to rust
/// code, to switch from a crude page table built in assembly. Calling this
/// function multiple times is pointless, as it will leak the currently active
/// page table.
///
/// # Safety
///
/// Technically, this function is unsafe, as it out of the blue uses a random
/// virtual page as a temporary page (page `0xCAFE_BABE` to be precise). If some
/// kernel section or multiboot info intersects this page, something will happen
/// when switching to the new table. Maybe just a panic, maybe nothing, maybe
/// UB. TODO: figure out which.
pub fn remap_kernel<A: FrameAllocator>(
    active_table: &mut ActivePageTable,
    allocator: &mut A,
    temp_page: &mut TemporaryPage,
    boot_info: &BootInformation,
) {
    use multiboot2::ElfSectionFlags;

    let mut new_table = {
        let frame = allocator.allocate().expect("out of memory");
        InactivePageTable::new(frame, active_table, temp_page, allocator)
    };
    active_table.with(&mut new_table, temp_page, |mapper| {
        let section_tag = boot_info.elf_sections_tag().expect("no sections tag");

        for section in section_tag.sections() {
            if !section.is_allocated() {
                continue;
            }
            assert_eq!(
                section.start_address() as usize % PAGE_SIZE,
                0,
                "section {} is not page aligned",
                section.name()
            );
            let mut flags = EntryFlags::empty();
            if section.flags().contains(ElfSectionFlags::ALLOCATED) {
                flags |= EntryFlags::PRESENT;
            }
            if section.flags().contains(ElfSectionFlags::WRITABLE) {
                flags |= EntryFlags::WRITABLE;
            }
            if !section.flags().contains(ElfSectionFlags::EXECUTABLE) {
                flags |= EntryFlags::NO_EXECUTE;
            }
            let start_frame = Frame::containing_address(section.start_address() as usize);
            let end_frame = Frame::containing_address(section.end_address() as usize - 1);
            for frame in Frame::range_inclusive(start_frame, end_frame) {
                mapper.identity_map(frame, flags, allocator);
            }
        }

        let vga_buffer_frame = Frame::containing_address(0xb8000);
        mapper.identity_map(vga_buffer_frame, EntryFlags::WRITABLE, allocator);

        let boot_info_start = Frame::containing_address(boot_info.start_address());
        let boot_info_end = Frame::containing_address(boot_info.end_address() - 1);
        for frame in Frame::range_inclusive(boot_info_start, boot_info_end) {
            mapper.identity_map(frame, EntryFlags::PRESENT, allocator);
        }
    });

    let old_table = active_table.switch(new_table);

    // In out initialization assembly code we had stack and initial page tables
    // laid out like this:
    // * p4_table (1 page)
    // * p3_table (1 page)
    // * p2_table (1 page)
    // * stack (multiple pages)
    // Now we can reuse p2 and p3 tables as extra stack space, and also unmap page
    // that corresponded to p4 table to get a stack guard page.
    let old_p4_page = Page::containing_address(old_table.p4_frame.start_address());
    active_table.unmap(old_p4_page, allocator);

    //println!("guard page at {:#x}", old_table.p4_frame.start_address());
    //loop {}
}
