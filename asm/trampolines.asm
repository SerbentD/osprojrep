global kernel_process_trampoline
global kernel_process_suspend
global kernel_process_end
extern kernel_process_post_suspend
extern stack_top

section .text
bits 64
kernel_process_trampoline:
    ;mov word [0x0], 0
    ; params in rdi, start in rsi, stack in rdx
    ; init stack
    mov rsp, rdx
    ; tail call start
    ; first param was and stays in rdi
    jmp rsi


kernel_process_suspend:
    mov [rdi + 0x00], rax
    mov [rdi + 0x08], rbx
    mov [rdi + 0x10], rcx
    mov [rdi + 0x18], rdx
    mov [rdi + 0x20], rsi
    mov [rdi + 0x28], rdi
    mov [rdi + 0x30], rbp
    mov [rdi + 0x38], rsp
    mov [rdi + 0x40], r8
    mov [rdi + 0x48], r9
    mov [rdi + 0x50], r10
    mov [rdi + 0x58], r11
    mov [rdi + 0x60], r12
    mov [rdi + 0x68], r13
    mov [rdi + 0x70], r14
    mov [rdi + 0x78], r15
    pushf
    pop rax
    mov [rdi + 0x80], rax
    mov qword [rdi + 0x88], .return_point
    jmp kernel_process_post_suspend
.return_point:
    ret


kernel_process_end:
    mov rsp, stack_top
    jmp rsi
