global start
global stack_top
extern long_mode_start

section .text
bits 32
start:
    mov esp, stack_top
    ; save Multiboot info pointer to edi
    ; rust functions take first parameter from edi,
    ; so this will be essentially passed to rust main
    mov edi, ebx
    ; also tell rust where to find the stack,
    ; because we need to know that when we set up interrupt stacks
    mov esi, stack_top

    call clear_screen

    mov edx, message_start
    call print_line

    ; check for multiboot magic value
    cmp eax, 0x36d76289
    jne no_multiboot

    ; check for cpuid support, based on code in
    ; https://wiki.osdev.org/Setting_Up_Long_Mode#Detection_of_CPUID
    pushfd
    pop eax
    mov ecx, eax
    xor eax, 1 << 21
    push eax
    popfd
    pushfd
    pop eax
    push ecx
    popfd
    xor eax, ecx
    jz no_cpuid

    ; check for long mode support
    ; test if extended processor info in available
    mov eax, 0x80000000
    cpuid
    cmp eax, 0x80000001
    jb no_long_mode
    ; use extended info to test if long mode is available
    mov eax, 0x80000001
    cpuid
    test edx, 1 << 29
    jz no_long_mode

    ; all checks passed
    mov edx, message_checks_passed
    call print_line

    ; set up identity mapping for first some megabytes of memory
    mov eax, p3_table
    or eax, 0b11 ; present + writable
    mov [p4_table], eax
    ; map first P3 entry to P2 table
    mov eax, p2_table
    or eax, 0b11 ; present + writable
    mov [p3_table], eax
    mov ecx, 0
.map_p2_table:
    ; map ecx-th P2 entry to a huge page that starts at address 2MiB*ecx
    mov eax, 0x200000  ; 2MiB
    mul ecx            ; start address of ecx-th page
    or eax, 0b10000011 ; present + writable + huge
    mov [p2_table + ecx * 8], eax ; map ecx-th entry
    inc ecx            ; increase counter
    cmp ecx, 512       ; if counter == 512, the whole P2 table is mapped
    jne .map_p2_table  ; else map the next entry

    ; recursively map p4 table
    mov eax, p4_table
    or eax, 3 ; present + writable
    mov [p4_table + 511 * 8], eax

    ; enable paging
    mov eax, p4_table
    mov cr3, eax
    ; enable PAE-flag in cr4 (Physical Address Extension)
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax
    ; set the long mode bit in the EFER MSR (model specific register)
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr
    ; enable paging in the cr0 register
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

    ; load global descriptor table
    lgdt [gdt64.pointer]

    jmp gdt64.code:long_mode_start

no_multiboot:
    mov edx, message_no_multiboot
    call print_line
    hlt

no_cpuid:
    mov edx, message_no_cpuid
    call print_line
    hlt

no_long_mode:
    mov edx, message_no_long_mode
    call print_line
    hlt

clear_screen:
    push ebx
    mov ebx, 0xb8000
.clear_loop:
    mov word [ebx], 0
    add ebx, 2
    cmp ebx, 0xb8000 + 2 * 80 * 25
    jl .clear_loop
    pop ebx
    ret

print_line:
    push ebx
    push ecx
    mov ch, 0xf
    xor ebx, ebx
    mov bl, [print_line_index]
    add byte [print_line_index], 1
    imul ebx, 160
    add ebx, 0xb8000
.print_loop:
    cmp byte [edx], 0
    je .loop_end
    mov cl, [edx]
    mov word [ebx], cx
    add ebx, 2
    add edx, 1
    jmp .print_loop
.loop_end:
    pop ecx
    pop ebx
    ret

section .data
    print_line_index: db 0

section .rodata
    message_start: db "Booting Spark OS", 0
    message_no_multiboot: db "Error: no multiboot (shouldn't happen, since we are using grub?)", 0
    message_no_cpuid: db "Error: no cpuid", 0
    message_no_long_mode: db "Error: no long mode", 0
    message_checks_passed: db "All checks passed", 0
    message_ok: db "OK", 0
    gdt64:
        dq 0 ; zero entry
    .code: equ $ - gdt64
        dq (1<<43) | (1<<44) | (1<<47) | (1<<53) ; code segment
    .pointer:
        dw $ - gdt64 - 1
        dq gdt64

section .bss
align 4096
p4_table:
    resb 4096
p3_table:
    resb 4096
p2_table:
    resb 4096
stack_bottom:
    resb 4096 * 64
stack_top:
