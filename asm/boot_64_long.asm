extern stack_top
extern rust_syscall_entry_point
extern rust_timer_handler
extern rust_keyboard_handler
global timer_trampoline
global keyboard_trampoline
global long_mode_start
global syscall_return
global kernel_process_return

section .text
bits 64
long_mode_start:
    ; load 0 into all data segment registers
    mov ax, 0
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    ; prepare to handle syscalls
    ; set IA32_LSTAR - address of handler function
    mov ecx, 0xC0000082
    mov rdx, syscall_trampoline
    mov rax, syscall_trampoline
    shr rdx, 32
    wrmsr
    ; set IA32_SFMASK - clear interrupt flag when handling syscall
    mov ecx, 0xC0000084
    xor rax, rax,
    or rax, 1 << 9
    wrmsr
    ; set IA32_STAR - code segment selector for syscall handler and return
    mov ecx, 0xC0000081
    xor rax, rax
    mov rdx, (8 << 16) | 8
    wrmsr
    ; enable syscalls in IA32_EFER
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1
    wrmsr

    extern rust_main
    call rust_main
    hlt

timer_trampoline:
    mov [old_rax], rax
    mov [old_rbx], rbx
    mov [old_rcx], rcx
    mov [old_rdx], rdx
    mov [old_rsi], rsi
    mov [old_rdi], rdi
    mov [old_rbp], rbp
    ; mov [old_rsp], rsp
    mov [old_r8], r8
    mov [old_r9], r9
    mov [old_r10], r10
    mov [old_r11], r11
    mov [old_r12], r12
    mov [old_r13], r13
    mov [old_r14], r14
    mov [old_r15], r15
    mov rax, [rsp]
    mov [old_rip], rax
    mov rax, [rsp + 16]
    mov [old_rflags], rax
    mov rax, [rsp + 24]
    mov [old_rsp], rax
    mov rdi, old_processor_state
    call rust_timer_handler

keyboard_trampoline:
    mov [old_rax], rax
    mov [old_rbx], rbx
    mov [old_rcx], rcx
    mov [old_rdx], rdx
    mov [old_rsi], rsi
    mov [old_rdi], rdi
    mov [old_rbp], rbp
    ; mov [old_rsp], rsp
    mov [old_r8], r8
    mov [old_r9], r9
    mov [old_r10], r10
    mov [old_r11], r11
    mov [old_r12], r12
    mov [old_r13], r13
    mov [old_r14], r14
    mov [old_r15], r15
    mov rax, [rsp]
    mov [old_rip], rax
    mov rax, [rsp + 16]
    mov [old_rflags], rax
    mov rax, [rsp + 24]
    mov [old_rsp], rax
    mov rdi, old_processor_state
    call rust_keyboard_handler

syscall_trampoline:
    mov [old_rip], rcx
    mov [old_rflags], r11
    mov [old_rax], rax
    mov [old_rbx], rbx
    mov [old_rcx], rcx
    mov [old_rdx], rdx
    mov [old_rsi], rsi
    mov [old_rdi], rdi
    mov [old_rbp], rbp
    mov [old_rsp], rsp
    mov [old_r8], r8
    mov [old_r9], r9
    mov [old_r10], r10
    mov [old_r11], r11
    mov [old_r12], r12
    mov [old_r13], r13
    mov [old_r14], r14
    mov [old_r15], r15
    mov [index], rax
    mov [param1], rdi
    mov [param2], rsi
    mov [param3], rdx
    mov [param4], r8
    mov rsp, stack_top
    mov rdi, syscall_params
    call rust_syscall_entry_point

syscall_return:
    mov rax, [rdi + 0x00]
    mov rbx, [rdi + 0x08]
    ; mov rcx, [rdi + 0x10]
    mov rdx, [rdi + 0x18]
    mov rsi, [rdi + 0x20]
    ; mov rdi, [rdi + 0x28]
    mov rbp, [rdi + 0x30]
    mov rsp, [rdi + 0x38]
    mov r8, [rdi + 0x40]
    mov r9, [rdi + 0x48]
    mov r10, [rdi + 0x50]
    ; mov r11, [rdi + 0x58]
    mov r12, [rdi + 0x60]
    mov r13, [rdi + 0x68]
    mov r14, [rdi + 0x70]
    mov r15, [rdi + 0x78]
    ; prep for sysret
    mov rcx, [rdi + 0x88] ; old rip
    mov r11, [rdi + 0x80] ; old flags
    or r11, 1 << 9 ; enable interrupts
    ; finally, restore rdi, and return
    mov rdi, [rdi + 0x28]
    ; for some reason nasm doesn't want to assemble this with with rex.w
    ; prefix, which would make it return to long mode instead of compatibility
    ; mode, so we just write the instruction bytes by hand
    ; sysret
    db 0x48, 0x0f, 0x07

kernel_process_return:
    mov rax, [rdi + 0x80]
    push rax
    popf
    mov rax, [rdi + 0x00]
    mov rbx, [rdi + 0x08]
    mov rcx, [rdi + 0x10]
    mov rdx, [rdi + 0x18]
    mov rsi, [rdi + 0x20]
    ; mov rdi, [rdi + 0x28]
    mov rbp, [rdi + 0x30]
    mov rsp, [rdi + 0x38]
    mov r8, [rdi + 0x40]
    mov r9, [rdi + 0x48]
    mov r10, [rdi + 0x50]
    mov r11, [rdi + 0x58]
    mov r12, [rdi + 0x60]
    mov r13, [rdi + 0x68]
    mov r14, [rdi + 0x70]
    mov r15, [rdi + 0x78]
    mov rax, [rdi + 0x88]
    push rax
    mov rdi, [rdi + 0x28]
    ret


section .data
syscall_params:
    index: dq 0
    param1: dq 0
    param2: dq 0
    param3: dq 0
    param4: dq 0
old_processor_state:
    old_rax: dq 0
    old_rbx: dq 0
    old_rcx: dq 0
    old_rdx: dq 0
    old_rsi: dq 0
    old_rdi: dq 0
    old_rbp: dq 0
    old_rsp: dq 0
    old_r8: dq 0
    old_r9: dq 0
    old_r10: dq 0
    old_r11: dq 0
    old_r12: dq 0
    old_r13: dq 0
    old_r14: dq 0
    old_r15: dq 0
    old_rflags: dq 0
    old_rip: dq 0
