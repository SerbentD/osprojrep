LINKER_64 := ld -m elf_x86_64
NASM := nasm
GRUB_MKRESCUE := grub-mkrescue


.PHONY: rust_kernel_64 clean run-64 iso-64
# no reason to remove these
.PRECIOUS: bin/%/isofiles/boot/kernel.bin bin/%/isofiles/boot/grub/grub.cfg

run-64: iso-64
	qemu-system-x86_64 -cdrom bin/64/spark.iso

debug-64: iso-64
	qemu-system-x86_64 -d int -no-reboot -cdrom bin/64/spark.iso

iso-64: bin/64/spark.iso

clean:
	@echo how the fuck do you clean shit

rust_kernel_64: bin/64/programs/args.bin bin/64/programs/cat.bin bin/64/programs/div.bin bin/64/programs/echo.bin bin/64/programs/sum.bin
	RUST_TARGET_PATH=$(shell pwd) xargo build --target=x86_64-spark

bin/64/programs/%.bin: programs/%.s
	nasm $< -o $@

bin/64/kernel.bin: linker.ld bin/64/multiboot_header.o bin/64/boot_long.o bin/64/boot.o bin/64/trampolines.o rust_kernel_64 bin/64/programs/args.bin bin/64/programs/cat.bin bin/64/programs/div.bin bin/64/programs/echo.bin bin/64/programs/sum.bin | bin/64
	$(LINKER_64) -n --gc-sections -o $@ -T linker.ld \
		bin/64/multiboot_header.o bin/64/boot_long.o bin/64/boot.o bin/64/trampolines.o \
		target/x86_64-spark/debug/libspark.a

bin/64/boot.o: asm/boot_64.asm
bin/64/boot_long.o: asm/boot_64_long.asm
bin/64/multiboot_header.o: asm/multiboot_header.asm
bin/64/trampolines.o: asm/trampolines.asm

bin/64/%.o: | bin/64/programs
	$(NASM) -f elf64 $^ -o $@

bin/%/spark.iso: bin/%/isofiles/boot/kernel.bin bin/%/isofiles/boot/grub/grub.cfg
# I'm sorry about that patsubst (not really).
	$(GRUB_MKRESCUE) -o $@ $(patsubst %/spark.iso,%/isofiles,$@)

bin/%/isofiles/boot/kernel.bin: bin/%/kernel.bin | bin/%/isofiles/boot
	cp $< $@

bin/%/isofiles/boot/grub/grub.cfg: grub/grub.cfg | bin/%/isofiles/boot/grub
	cp $< $@

bin/64/isofiles/boot/grub: | bin/64/isofiles/boot
bin/64/isofiles/boot: | bin/64/isofiles
bin/64/isofiles: | bin/64
bin/64/programs: | bin/64
bin/64: | bin

bin bin/64 bin/64/isofiles bin/64/isofiles/boot bin/64/isofiles/boot/grub bin/64/programs:
	mkdir "$@"
