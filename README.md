# Spark

A toy operating system for operating systems university course.

File system is implemented sepparately, and is hosted here: https://github.com/jDomantas/spark-fs.

If you do not want to build Spark from source, you can find prebuild `spark.iso`
in [bitbucket downloads section](https://bitbucket.org/SerbentD/osprojrep/downloads/).

## Build instructions

If you are on Ubuntu or something similar enough:

* `sudo apt install build-essential qemu nasm xorriso grub-common grub-pc-bin mtools`
* [Install rustup](https://www.rustup.rs/)
* `rustup toolchain install nightly-2018-04-08`
* `rustup default nightly-2018-04-08`
* `rustup component add rust-src`
* `cargo install xargo`

Now you should have the following executables on your path:

* `nasm`
* `ld`
* `grub-mkrescue`
* `cargo`
* `rustc`
* `xargo`
* `qemu-system-x86_64`

Now you can:

* Run `make iso-64` to create boot image (which will be place in `bin/64/spark.iso`)
* Run `make run-64` to run spark in qemu.

## Documentation

If you want a more convenient way to browse the project, you can run
`cargo doc --open` - it will build the docs from doc comments in the code and
open it in your browser.

However, this won't display docs for private items - so you won't be able to
read documentation for most modules' internal implementations. If you want to
see those too, you can use `cargo rustdoc -- --document-private-items`. This
will add some useless stuff too though - for example, imports for each module
will be listed, because technically they are private items. Sadly, at the time
of writing there doesn't seem to exist a good way to strip those.

Currently most of the code is undocumented and quite messy, and this won't change.

## Shell

There are 12 terminals that you can switch to using keys `F1` - `F12`. In each
one you can run one program at a time. Run a program by writing its name
(listed in the next section), and optionally providing it an argument, like this:
`args foobar`. There's one builtin command that you can run which is not a program
found in the file system - `stat`, which prints out memory usage.

## Programs

Currently the OS uses RAM as the backing storage of its file system. On boot, it
initializes the file system with 5 programs:

* `args` - prints out its command line argument
* `cat` - prints out contents of given file
* `div` - print numbers from 1 to infinity
* `sum` - reads two numbers from stdin, prints sum to stdout
* `echo` - reads lines from stdin and prints them to stdout

There's also another file, called `text`, so that you could test `cat` with it:
`cat text`.

## Program format and system calls

Program format is fairly simple:

* Eight bytes containing a string `"sparkexe"`
* Eight bytes for program's code section length, little endian.
* Eight bytes for program's data section length, little endian.
* Program's code section.
* Program's data section. If file ends before end of this section, remaining
    data section will be filled with zeroes.

When program is loaded, it's code section will be placed in memory starting at
256 MB mark, and data section will be placed starting at 512 MB mark. Program
will also be given 1 MB of stack, which is placed just before data section.

At the program start a little bit of stack will be used to place command line
parameter, its address will be written to `rax`, and length of the parameter
will be stored in `rbx` (there can only be one parameter - shell does not
perform any splitting and simply passes the whole string that goes after program
name).

Programs are not interpreted by the operating system - they are simply loaded
into memory, and then processor jumps to that memory. Therefore, programs can
use all features available to `x86-64` processor, except:

* Interrupt instructions
* Privileged instructions (programs run in user mode)
* Registers `rcx` and `r11`. Due to an implementation deficiency, the kernel
    won't restore values of those registers when jumping to user code. Therefore,
    if the timer interrupts program execution of the program, from the program's
    perspective `rcx` and `r11` will spontaneously change values, which makes
    those two registers useless for the program.

Program can interact with kernel usign system calls. To execute a system call a
program has to:

* Write syscall id to `rax`
* Pass arguments in registers `rdi`, `rsi`, `rdx`. If syscall takes 0, 1, or 2
    parameters, only first 0, 1, or 2 registers will be read by the kernel.
* Execute instruction `syscall`.

Syscall result will be written to `rax`. If there was an error, `rax` will
contain value `-1`, and an error code for more details can be obtained with
"Last error" syscall. Syscall will not change any other registers (aside from
`rcx` and `r11`, as mentioned earlier).

Syscall list:

| Id | Name        | Params                            | Result                                  | Description                        |
|----|-------------|-----------------------------------|-----------------------------------------|------------------------------------|
| 0  | Exit        | exit_code                         | -                                       | Exit process with given code       |
| 2  | Create file | path_address, path_length         | File descriptor                         | Create a file and open for reading |
| 3  | Write       | fd, buffer_address, buffer_length | Bytes written                           | Write to file descriptor           |
| 4  | Open file   | path_address, path_length         | File descriptor                         | Open file for reading              |
| 5  | Read        | fd, buffer_address, buffer_length | Bytes read (0 if reached end of stream) | Read from file descriptor          |
| 6  | Close       | fd                                | -                                       | Close file descriptor              |
| 7  | Last error  |                                   | Error code                              | Get error code of last syscall     |


## Adding more programs

Currently the file system is very simple - it allows storing up to 16 files, each
up to 1 MB in size. You can add more by adding an entry to `PROGRAMS` array in
`src/lib.rs` - first field of the tuple is the file name, and the second is the
contents. Note that you can use Rust macro `include_bytes!` to conveniently
include binary files. Any file can be included this way, not just executable
programs.
