bits 64

db "sparkexe"
dq code_end - code_start
dq 4096 ; data_end - data_start

org 10000000h

code_start:
    ; write message to stdout
    mov rsi, rax
    mov rdx, rbx
    mov rax, 3
    ;mov rdi, 0
    ;mov rsi, 20000000h - data_start + msg
    ;mov rdx, path - msg
    syscall
    jmp halt

echo:
    mov rax, 5
    mov rdi, 0
    mov rsi, 20000000h
    mov rdx, 1000
    syscall

    mov rdx, rax
    mov rax, 3
    syscall
    jmp echo

    ; exit
;    mov rax, 0
;    mov rdi, 0
;    syscall

;foo: jmp foo
    
    ; open file
    mov rax, 4
    mov rdi, 20000000h - data_start + path
    mov rsi, 4
    syscall
    ; check if open succeeded
    cmp rax, 0
    je open_failed
    ; remember fd
    mov r15, rax
    ; anounce success
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + msg3
    mov rdx, msg4 - msg3
    syscall
    ; read 8 bytes
    call read_bytes
    ; close
    mov rax, 6
    mov rdi, r15
    syscall
    jmp halt

read_failed:
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + msg4
    mov rdx, msg5 - msg4
    syscall
    jmp halt

open_failed:
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + msg2
    mov rdx, msg3 - msg2
    syscall
halt:
    ;mov rax, 3
    ;mov rdi, 0,
    ;mov rsi, 20000000h - data_start + msg5
    ;mov rdx, msg6 - msg5
    ;syscall
    mov rax, 0
    mov rdi, 0
    syscall
    jmp halt

read_bytes:
    mov rax, 5
    mov rdi, r15
    mov rsi, 20000000h - data_start + buf_start
    mov rdx, buf_end - buf_start
    syscall
    ; check if read failed
    cmp rax, -1
    je read_failed
    ; print bytes as string
    mov rax, 3
    mov rdi, 0
    syscall
    ret
code_end:

data_start:
    msg: db "Writing to stdout", 10
    path: db "test"
    msg2: db "cannot open", 10
    msg3: db "opened file `test`", 10
    msg4: db "read failed", 10
    msg5: db "exiting", 10
    msg6:
    buf_start: db 0,0,0,0,0,0,0,0
    buf_end:
data_end:
