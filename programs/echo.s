bits 64

db "sparkexe"
dq code_end - code_start
dq 4096 ; data_end - data_start

org 10000000h

code_start:
    ; write message to stdout
    mov rsi, rax
    mov rdx, rbx
    mov rax, 3

echo:
    mov rax, 5
    mov rdi, 0
    mov rsi, 20000000h
    mov rdx, 1000
    syscall

    cmp rax, 2
    jne not_end
    cmp byte [rsi], 'q'
    jne not_end
    cmp byte [rsi + 1], 10
    jne not_end
    jmp halt
not_end:

    mov rdx, rax
    mov rax, 3
    syscall
    jmp echo

halt:
    ;mov rax, 3
    ;mov rdi, 0,
    ;mov rsi, 20000000h - data_start + msg5
    ;mov rdx, msg6 - msg5
    ;syscall
    mov rax, 0
    mov rdi, 0
    syscall
    jmp halt
code_end:

data_start:
    msg: db "Writing to stdout", 10
    path: db "test"
    msg2: db "cannot open", 10
    msg3: db "opened file `test`", 10
    msg4: db "read failed", 10
    msg5: db "exiting", 10
    msg6:
    buf_start: db 0,0,0,0,0,0,0,0
    buf_end:
data_end:
