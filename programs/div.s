bits 64

db "sparkexe"
dq code_end - code_start
dq 4096 ; data_end - data_start

org 10000000h

code_start:
    

    

    mov r15, 1
search_loop:
    call print_num
    inc r15
    jmp search_loop

print_num:
    push rax
    push rdx
    push rdi
    push rsi
    mov r14, 20000000h - data_start + divisor_num
    call print_rec
    mov byte [r14], 10
    inc r14
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + divisor
    mov rdx, r14
    sub rdx, 20000000h - data_start + divisor
    syscall
    pop rsi
    pop rdi
    pop rdx
    pop rax
    ret

print_rec:
    push rax
    push rdx
    push r15
    xor rdx, rdx
    mov rax, r15
    mov r15, 10
    div r15
    cmp rax, 0
    je no_rec
    mov r15, rax
    call print_rec
no_rec:
    add rdx, '0'
    mov [r14], dl
    inc r14
    pop r15
    pop rdx
    pop rax
    ret

halt:
    ;mov rax, 3
    ;mov rdi, 0,
    ;mov rsi, 20000000h - data_start + msg5
    ;mov rdx, msg6 - msg5
    ;syscall
    mov rax, 0
    mov rdi, 0
    syscall
    jmp halt
code_end:

data_start:
    bad_num: db "invalid number", 10
    bad_num_end: 
    divisor: db "divisor: "
    divisor_num: db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    
data_end:
