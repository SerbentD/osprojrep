bits 64

db "sparkexe"
dq code_end - code_start
dq 4096 ; data_end - data_start

org 10000000h

code_start:
    call read_number
    mov r14, rax
    call read_number
    mov r15, rax
    add r15, r14
    call print_num
    jmp exit

print_num:
    push rax
    push rdx
    push rdi
    push rsi
    mov r14, 20000000h - data_start + divisor_num
    call print_rec
    mov byte [r14], 10
    inc r14
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + divisor
    mov rdx, r14
    sub rdx, 20000000h - data_start + divisor
    syscall
    pop rsi
    pop rdi
    pop rdx
    pop rax
    ret

print_rec:
    push rax
    push rdx
    push r15
    xor rdx, rdx
    mov rax, r15
    mov r15, 10
    div r15
    cmp rax, 0
    je .no_rec
    mov r15, rax
    call print_rec
.no_rec:
    add rdx, '0'
    mov [r14], dl
    inc r14
    pop r15
    pop rdx
    pop rax
    ret

read_line:
    push rax
    push rdi
    push rsi
    push rdx
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + enter_msg
    mov rdx, enter_msg_end - enter_msg
    syscall
    mov rax, 5
    mov rdi, 0
    mov rsi, 20000000h - data_start + entry_buf
    mov rdx, entry_buf_end - entry_buf
    syscall
    ; mov byte [20000000h - data_start + entry_buf + rax - 1], 0
    pop rdx
    pop rsi
    pop rdi
    pop rax
    ret

read_number:
    push rax
    push rdx
    push r8
    push r15
    call read_line
    mov rax, 0
    mov rdx, 0
    mov r8, 20000000h - data_start + entry_buf

.parse_loop:
    cmp byte [r8], 10
    je .break
    mov r15, 10
    mul r15
    mov rdx, 0
    mov dl, byte [r8]
    sub dl, '0'
    inc r8
    add rax, rdx
    jmp .parse_loop
.break:
    pop r15
    pop r8
    pop rdx
    add rsp, 8
    ret

exit:
    mov rax, 0
    mov rdi, 0
    syscall
code_end:

data_start:
    enter_msg: db "Enter number: "
    enter_msg_end:
    entry_buf: db 0,0,0,0,0,0,0,0,0,0,0
    entry_buf_end:
    bad_num: db "invalid number", 10
    bad_num_end: 
    divisor: db "Total: "
    divisor_num: db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    
data_end:
