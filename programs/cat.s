bits 64

db "sparkexe"
dq code_end - code_start
dq 4096 ; data_end - data_start

org 10000000h

code_start:
    ; use arg as file name
    mov rdi, rax
    mov rsi, rbx
    ; open
    mov rax, 4
    syscall
    ; check if open succeeded
    cmp rax, -1
    je open_failed
    ; opened a file successfully
    ; remember fd
    mov r15, rax
    ; read
read_loop:
    mov rax, 5
    mov rdi, r15
    mov rsi, 20000000h - data_start + buf_start
    mov rdx, buf_end - buf_start
    syscall
    ; check result
    cmp rax, -1
    je read_failed
    cmp rax, 0
    je exit
    ; got `rax` bytes, write to stdout
    mov rdx, rax
    mov rax, 3
    mov rdi, 0
    syscall
    jmp read_loop

read_failed:
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + read_fail_msg
    mov rdx, read_fail_msg_end - read_fail_msg
    syscall
    jmp exit

open_failed:
    mov rax, 3
    mov rdi, 0
    mov rsi, 20000000h - data_start + open_fail_msg
    mov rdx, open_fail_msg_end - open_fail_msg
    syscall
    jmp exit

exit:
    mov rax, 0
    mov rdi, 0
    syscall
    jmp exit

code_end:

data_start:
    buf_start: db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    buf_end:
    read_fail_msg: db 10, "read failure", 10
    read_fail_msg_end:
    open_fail_msg: db "failed to open file", 10
    open_fail_msg_end:
data_end:
